<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-16 17:11
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Mapper\CvMapper;
use CvCreator\CvCreatorBundle\Form\Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class description:
 *
 * @Route("/cv")
 * @author Burim
 */
class CvController extends Controller implements InitializableControllerInterface
{
    private $cv;

    /**
     * Check if this user is allowed to access this CV before every action
     *
     * @param Request $request
     * @param SecurityContextInterface $security_context
     * @throws NotFoundHttpException
     * @return Response
     */
    public function initialize(Request $request, SecurityContextInterface $security_context)
    {
        $router = $this->get("router");
        $route = $router->match($this->getRequest()->getPathInfo());

        if (!empty($route['cv_id'])) {
            $cv = $this->getCv($route['cv_id']);
            if (!$cv instanceof Entity\Cv) {
                throw new NotFoundHttpException("Page not found");
            } else {
                $this->cv = $cv;
            }
        }
    }

    private function getCvUser()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        /* this code creates a temporary user
        if (!$user instanceof Entity\User) {

            $em = $this->getDoctrine()->getManager();

            $session = $this->getRequest()->getSession();
            $userId = (int)$session->get('userId');

            if (!empty($userId)) {
                $user = $em->getRepository('CvCreatorBundle:User')->find($userId);
            }

            if (!$user instanceof Entity\User) {
                $user = new Entity\User();
                $user->setIsActive(false);
                $user->setCreatedate(time());
                $user->setGender('NotKnown');

                $em->persist($user);
                $em->flush();

                $session->set('userId', $user->getId());
            }

            $this->get('security.context')->getToken()->setUser($user);
        }
        */

        return $user;
    }

    private function getCv($cv_id = null)
    {
        if (!empty($this->cv)) {
            return $this->cv;
        }

        $cv = null;

        /** @var $user Entity\User */
        $user = $this->getCvUser();

        $em = $this->getDoctrine()->getManager();
        if (!empty($cv_id) && $user instanceof Entity\User) {
            /** @var $cvById Entity\Cv */
            $cvById = $em->getRepository('CvCreatorBundle:Cv')->find($cv_id);
            if ($cvById instanceof Entity\Cv && $cvById->getUser()->getId() == $user->getId()) {
                $cv = $cvById;
            }
        } else {
            $cv = $em->getRepository('CvCreatorBundle:Cv')->findOneByUser($user);

            if ($cv == null) {
                $cv = new Entity\Cv();
                $cv->setUser($user);
                $cv->setCreatedate(time());
                $cv->setUpdatedate(time());

                $em->persist($cv);
                $em->flush();
            }
        }

        return $cv;
    }

    /**
     * @Route("/", name="_cv")
     */
    public function indexAction()
    {
        return $this->render('CvCreatorBundle:Default:index.html.twig', array('messages' => array()));
    }

    /**
     * @Route("/create", name="_cv_create")
     */
    public function createAction()
    {
        $cv_id = null;
        $cv = $this->getCv($cv_id);
        $error = '';
        $em = $this->getDoctrine()->getManager();
        /** @var $user Entity\User */
        $user = $this->getCvUser();

        $personalData = $em->getRepository('CvCreatorBundle:CvPersonalData')->findOneByCv($cv);

        if (!$personalData instanceof Entity\CvPersonalData) {
            $personalData = new Entity\CvPersonalData();
            $personalData->setCv($cv);
            $personalData->setCreatedate(time());
            $personalData->setUpdatedate(time());
            $personalData->setFirstname($user->getFirstname());
            $personalData->setLastname($user->getLastname());
            $personalData->setEmail($user->getEmail());

            $em->persist($personalData);
            $em->flush();
        }

        $cv_id = $cv->getId();
        $cvMapper = new CvMapper($em, $this->container, $cv);
        $sections = $cvMapper->getSections();

        return $this->render(
            'CvCreatorBundle:Cv:create.html.twig',
            array(
                'messages' => array(),
                'cv_id' => $cv_id,
                'sections' => $sections,
                'error' => $error,

            )
        );
    }

    /**
     * @Route("/edit/{cv_id}/{part_type}/{section_id}/{part_id}", name="_cv_edit_item")
     */
    public function editAction($cv_id, $part_type, $section_id, $part_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $cvMapper = new CvMapper($em, $this->container, $cv);
        $section = $cvMapper->getSection($part_type, $section_id);

        $type = $this->get('cvcreator.type.' . $part_type);

        /** @var $part Entity\CvMapperItemInterface */
        $part = $em->getRepository('CvCreatorBundle:' . $section->getEntityName())->find($part_id);
        $form = $this->createForm($type, $part);

        return $this->render(
            'CvCreatorBundle:Form:cv_part_form.html.twig',
            array(
                'messages' => array(),
                'cv_id' => $cv_id,
                'part_type' => $part_type,
                'part_id' => $part_id,
                'form' => $form->createView(),
                'section' => $section
            )
        );
    }

    /**
     * @Route("/save/{cv_id}/{part_type}/{section_id}/{part_id}", name="_cv_save_item")
     */
    public function saveAction($cv_id, $part_type, $section_id, $part_id, Request $request)
    {
        $success = false;
        $type = $this->get('cvcreator.type.' . $part_type);
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $cvMapper = new CvMapper($em, $this->container, $cv);

        /** @var $section \CvCreator\CvCreatorBundle\Mapper\CvSectionMapper */
        $section = $cvMapper->getSection($part_type, $section_id);

        $em = $this->getDoctrine()->getManager();

        $postData = $request->get($part_type);
        if (empty($part_id)) {
            $part_id = $postData['id'];
        }

        if (!empty($part_id)) {
            $item = $em->getRepository('CvCreatorBundle:' . $section->getEntityName())->find($part_id);
        } else {
            $item = $this->get('cvcreator.entity.' . $part_type);

            if ($part_type == 'customsectionitem') {
                $sectionItem = $em->getRepository('CvCreatorBundle:CvSection')->find($section_id);
                if ($sectionItem instanceof Entity\CvSection) {
                    $section_id = $sectionItem->getId();
                    $item->setSection($sectionItem);
                }
            }

            $item->setCv($cv);
        }

        $form = $this->createForm($type, $item);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if (0 === (int)$item->getSortPosition()) {
                $sortPositionItem = $em->getRepository('CvCreatorBundle:' . $section->getEntityName())
                                       ->findBy(array('cv' => $cv), array('sortposition' => 'DESC'), 1);

                $sortPosition = !empty($sortPositionItem[0]) ? (int)$sortPositionItem[0]->getSortPosition() + 1 : 0;
                $item->setSortPosition($sortPosition);
            }

            if ($part_type == 'personaldata') {
                $item->preparePassPhoto($cv, $em);
            }

            if ($form->isValid()) {

                if (empty($part_id)) {
                    $data = $form->getData();
                    $em->persist($data);
                }

                $em->flush();
                $success = true;
            }
        }

        $template = $success ? 'saved' : 'form';

        return $this->render(
            'CvCreatorBundle:Form:cv_part_' . $template . '.html.twig',
            array(
                'messages' => array(),
                'cv_id' => (int)$cv_id,
                'part_type' => $part_type,
                'part_id' => (int)$part_id,
                'section_id' => (int)$section_id,
                'form' => $form->createView(),
                'is_post' => $request->isMethod('POST'),
                'section' => $section,
                'redirectToAdd' => !empty($_POST['save_and_insert']) ? true : false
            )
        );
    }

    /**
     * @Route("/add/{cv_id}/{part_type}/{section_id}", name="_cv_add_item")
     */
    public function addPartAction($cv_id, $part_type, $section_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $type = $this->get('cvcreator.type.' . $part_type);
        $dataObject = $this->get('cvcreator.entity.' . $part_type);

        if ($part_type == 'customsectionitem') {
            $section = $em->getRepository('CvCreatorBundle:CvSection')->find($section_id);
            if ($section instanceof Entity\CvSection) {
                $section_id = $section->getId();
                $dataObject->setSection($section);
            }
        }

        $dataObject->setCv($cv);

        $form = $this->createForm($type, $dataObject);

        $cvMapper = new CvMapper($em, $this->container, $cv);
        $section = $cvMapper->getSection($part_type, $section_id);

        return $this->render(
            'CvCreatorBundle:Form:cv_part_form.html.twig',
            array(
                'messages' => array(),
                'cv_id' => $cv_id,
                'section_id' => $section_id,
                'part_type' => $part_type,
                'part_id' => 0,
                'form' => $form->createView(),
                'section' => $section
            )
        );
    }

    /**
     * @Route("/move-part-down/{cv_id}/{part_type}/{section_id}/{part_id}/{way}", name="_cv_move_section_part")
     */
    public function moveSectionPartAction($cv_id, $part_type, $section_id, $part_id, $way)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $cvMapper = new CvMapper($em, $this->container, $cv);
        /** @var $section \CvCreator\CvCreatorBundle\Mapper\CvSectionMapper */
        $section = $cvMapper->getSection($part_type, $section_id);

        if ($part_type == 'customsectionitem') {
            $items = $em->getRepository('CvCreatorBundle:' . $section->getEntityName())
                ->findBy(array('cv' => $cv, 'section' => $section_id), array('sortposition' => 'ASC'));
        } else {
            $items = $em->getRepository('CvCreatorBundle:' . $section->getEntityName())
                ->findBy(array('cv' => $cv), array('sortposition' => 'ASC'));
        }

        $itemIndex = -1;
        $swapItemIndex = -1;
        foreach ($items as $index => $item) {
            if ($item->getId() == $part_id) {
                $itemIndex = $index;
                $swapItemIndex = $way == 'up' ? $index - 1 : $index + 1;
            }
            $item->setSortPosition($index);
        }

        if (!empty($items[$swapItemIndex]) && !empty($items[$itemIndex])) {
            $oldSortPosition = $items[$itemIndex]->getSortposition();
            $newSortPosition = $items[$swapItemIndex]->getSortposition();
            $items[$swapItemIndex]->setSortposition($oldSortPosition);
            $items[$itemIndex]->setSortposition($newSortPosition);
        }

        $em->flush();

        $response = new Response();
        $response->setStatusCode(200);

        return $response;
    }

    /**
     * @Route("/delete-part/{cv_id}/{part_type}/{section_id}/{part_id}", name="_cv_delete_part")
     */
    public function deletePartAction($cv_id, $part_type, $section_id, $part_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $cvMapper = new CvMapper($em, $this->container, $cv);
        /** @var $section \CvCreator\CvCreatorBundle\Mapper\CvSectionMapper */
        $section = $cvMapper->getSection($part_type, $section_id);
        $part = $em->getRepository('CvCreatorBundle:' . $section->getEntityName())->find($part_id);

        if (!empty($cv) && !empty($part)) {
            $em->remove($part);
            $em->flush();
        }

        $response = new Response();
        $response->setStatusCode(200);

        return $response;
    }

    /**
     * @Route("/generate/{cv_id}/{layout}/{format}/{inline}", name="_cv_generate")
     */
    public function generateAction($cv_id, $layout, $format, $inline)
    {
        $cv = $this->getCv($cv_id);

        $errors = array();
        $em = $this->getDoctrine()->getManager();

        /** @var $user Entity\User */
        $user = $this->getCvUser();

        $cv_id = $cv->getId();
        $cvMapper = new CvMapper($em, $this->container, $cv);
        $sections = $cvMapper->getSections();

        $html = $this->render(
            'CvCreatorBundle:Cv:generate.html.twig',
            array(
                'messages' => array(),
                'cv_id' => $cv_id,
                'sections' => $sections,
                'layout' => ucfirst($layout),
                'format' => $format
            )
        );

        $hash = md5($html->getContent());
        $userDir = $user->getId() . DIRECTORY_SEPARATOR . $cv_id;
        $outputFileName = 'CV-' . ucfirst($user->getFirstname()) . '-' . ucfirst($user->getLastname()) . '.' . $format;

        /** @var $source \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface */
        $source = $this->get('transformer.source.xhtml');
        try {
            $source->setUserDir($userDir)
                   ->setFileName($hash)
                   ->save($html->getContent(), $userDir);
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
        }

        /** @var $output \CvCreator\CvCreatorBundle\Transformer\Target\TargetInterface */
        $output = $this->get('transformer.target.' . strtolower($format));
        try {
            $content = $output->setUserDir($userDir)
                   ->setFileName($hash)
                   ->getContent($source, $layout);

            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent($content);
            $response->headers->set('Content-Type', $output->getMimeContentType());
            $response->headers->set(
                'Content-Disposition',
                ($inline == 1 ? 'inline' : 'attachment') . '; filename=' . $outputFileName
            );
            $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
            $response->headers->set('Expires', '0');
            $response->headers->set('Pragma', 'public');
            return $response;

        } catch (Exception $e) {
            $errors[] = $e->getMessage();
        }

        // show error page if the cv was not printed out
        return $this->render(
            'CvCreatorBundle:Default:error.html.twig',
            array(
                'errorMessage' => $this->get('translator')->trans("Ein Fehler ist aufgetretten!"),
            )
        );
    }

    /**
     * This method will be used to update a section by ajax
     *
     * @Route("/get-section/{cv_id}/{part_type}/{section_id}", name="_cv_get_section")
     */
    public function getSectionDataAction($cv_id, $part_type, $section_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $cvMapper = new CvMapper($em, $this->container, $cv);
        $section = $cvMapper->getSection($part_type, $section_id);

        return $this->render(
            'CvCreatorBundle:Cv:Create/section.html.twig',
            array(
                'messages' => array(),
                'cv_id' => $cv_id,
                'part_type' => $part_type,
                'section' => $section
            )
        );
    }

    /**
     * @Route("/attachment/{cv_id}/{attachment_id}/{thumb}", name="_cv_get_attachment")
     */
    public function getAttachmentAction($cv_id, $attachment_id, $thumb = false)
    {
        $response = new Response();
        $em = $this->getDoctrine()->getManager();

        /** @var $attachment \CvCreator\CvCreatorBundle\Entity\CvAttachment */
        $attachment = $em->getRepository('CvCreatorBundle:CvAttachment')->find($attachment_id);

        if (!$attachment instanceof Entity\CvAttachment) {
            $response->setStatusCode(404);
            return $response;
        }

        $fileContent = $attachment->getFileContent($thumb);

        if (empty($fileContent)) {
            $response->setStatusCode(404);
            return $response;
        }

        $response->setContent($fileContent);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', $attachment->getMimeContentType());
        $response->headers->set('Content-Disposition', 'inline; filename=' . $attachment->getFileName());
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Expires', '0');
        $response->headers->set('Pragma', 'public');

        // prints the HTTP headers followed by the content
        return $response;
    }

    /**
     * @Route("/application/{cv_id}/{layout}/{format}", name="_cv_application")
     */
    public function applicationAction($cv_id, $layout, $format, Request $request)
    {
        $type = $this->get('cvcreator.type.application');
        /** @var $user Entity\User */
        $user = $this->getCvUser();
        $cv = $this->getCv($cv_id);
        $errors = array();

        $application = new Entity\Application();
        $application->setCv($cv);
        $application->setUser($user);
        $form = $this->createForm($type, $application);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            $em = $this->getDoctrine()->getManager();



            $cv_id = $cv->getId();
            $cvMapper = new CvMapper($em, $this->container, $cv);
            $sections = $cvMapper->getSections();

            $html = $this->render(
                'CvCreatorBundle:Cv:generate.html.twig',
                array(
                    'messages' => array(),
                    'cv_id' => $cv_id,
                    'sections' => $sections,
                    'layout' => ucfirst($layout),
                    'format' => $format
                )
            );

            $hash = md5($html->getContent());
            $userDir = $user->getId() . DIRECTORY_SEPARATOR . $cv_id;
            $outputFileName = 'CV-' . ucfirst($user->getFirstname()) . '-' . ucfirst($user->getLastname()) . '.' . $format;

            /** @var $source \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface */
            $source = $this->get('transformer.source.xhtml');
            try {
                $source->setUserDir($userDir)
                    ->setFileName($hash)
                    ->save($html->getContent(), $userDir);
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }

            /** @var $output \CvCreator\CvCreatorBundle\Transformer\Target\TargetInterface */
            $output = $this->get('transformer.target.' . strtolower($format));
            try {
                $content = $output->setUserDir($userDir)->setFileName($hash)->getContent($source, $layout);

                /** @var $application Entity\Application */
                $application = $form->getData();

                // prepare email
                $txtEmail = $application->getMessage() . "\r\n\r\n";
                $htmlEmail = $application->getMessage() . "<br/><br/>";
                $attachment = \Swift_Attachment::newInstance($content, $outputFileName, $output->getMimeContentType());

                // send contact
                $message = \Swift_Message::newInstance()
                    ->setSubject($application->getSubject())
                    ->setFrom($user->getEmail(), $user->getFirstname() . ' ' . $user->getLastname())
                    ->setTo($application->getReceiverEmail(), $application->getReceiverName())
                    ->addPart($htmlEmail, 'text/html')
                    ->addPart($txtEmail, 'text/plain')
                    ->attach($attachment);

                // send email
                $this->get('mailer')->send($message);

                // save the sent application
                $application->setCv($cv);
                $application->setUser($user);
                $application->setDate(time());
                $application->setCvContent($html->getContent());

                $em->persist($application);
                $em->flush();

                // return success response
                return $this->render(
                    'CvCreatorBundle:Form:application.html.twig',
                    array(
                        'cv_id' => $cv->getId(),
                        'form' => $form->createView(),
                        'layout' => $layout,
                        'format' => $format,
                        'success' => true,
                        'message' => $this->get('translator')->trans("Die Bewerbung wurde erfolgreich gesendet!"),
                    )
                );

                return $response;

            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }

        } else {

            return $this->render(
                'CvCreatorBundle:Form:application.html.twig',
                array(
                    'cv_id' => $cv->getId(),
                    'form' => $form->createView(),
                    'layout' => $layout,
                    'format' => $format,
                    'success' => false,
                    'message' => ''
                )
            );

        }

        // show error page if the cv was not printed out
        return $this->render(
            'CvCreatorBundle:Default:error.html.twig',
            array(
                'errorMessage' => $this->get('translator')->trans("Ein Fehler ist aufgetretten!"),
            )
        );
    }

    /**
     * @Route("/{cv_id}/{section_id}", name="_cv_section_edit")
     */
    public function editSectionAction($cv_id, $section_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);
        $status = 'edit';

        $type = $this->get('cvcreator.type.section');

        // create new custom section
        if ($section_id == 0) {
            $customSection = new Entity\CvSection();
            $customSection->setCv($cv);
            $customSection->setFormType('customsectionitem');

            $sortPositionItem = $em->getRepository('CvCreatorBundle:CvSection')
                ->findBy(array('cv' => $cv), array('sortposition' => 'DESC'), 1);

            $sortPosition = !empty($sortPositionItem[0]) ? (int)$sortPositionItem[0]->getSortPosition() + 1 : 0;
            $customSection->setSortPosition($sortPosition);

            $em->persist($customSection);
            $em->flush();

            $section_id = $customSection->getId();
        }

        /** @var $part Entity\CvSection */
        $item = $em->getRepository('CvCreatorBundle:CvSection')->find($section_id);
        $form = $this->createForm($type, $item);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if (0 === (int)$item->getSortPosition()) {
                $sortPositionItem = $em->getRepository('CvCreatorBundle:CvSection')
                    ->findBy(array('cv' => $cv), array('sortposition' => 'DESC'), 1);

                $sortPosition = !empty($sortPositionItem[0]) ? (int)$sortPositionItem[0]->getSortPosition() + 1 : 0;
                $item->setSortPosition($sortPosition);
            }

            if ($form->isValid()) {
                if (empty($part_id)) {
                    $data = $form->getData();
                    $em->persist($data);
                }
                $em->flush();
                $status = 'saved';
            }
        }

        return $this->render(
            'CvCreatorBundle:Form:cv_section_form.html.twig',
            array(
                'messages' => array(),
                'cv_id' => $cv->getId(),
                'section_id' => $section_id,
                'form' => $form->createView(),
                'status' => $status,
            )
        );
    }

    /**
     * @Route("/edit_section_title/{cv_id}/{section_id}", name="_cv_update_section_title")
     */
    public function updateSectionTitleAction($cv_id, $section_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        /** @var $part Entity\CvSection */
        $item = $em->getRepository('CvCreatorBundle:CvSection')->find($section_id);

        $title = !empty($item) ? $item->getTitle() : '';

        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent($title);

        return $response;
    }

    /**
     * @Route("/move_section/{cv_id}/{section_id}/{way}", name="_cv_move_section")
     */
    public function moveSectionAction($cv_id, $section_id, $way)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $items = $em->getRepository('CvCreatorBundle:CvSection')
            ->findBy(array('cv' => $cv), array('sortposition' => 'ASC'));

        $itemIndex = -1;
        $swapItemIndex = -1;
        foreach ($items as $index => $item) {
            if ($item->getId() == $section_id) {
                $itemIndex = $index;
                $swapItemIndex = $way == 'up' ? $index - 1 : $index + 1;
            }
            $item->setSortPosition($index);
        }

        if (!empty($items[$swapItemIndex]) && !empty($items[$itemIndex])) {
            $oldSortPosition = $items[$itemIndex]->getSortposition();
            $newSortPosition = $items[$swapItemIndex]->getSortposition();

            // do not override the personal data and attachment sort position
            if ($newSortPosition != 0 && $newSortPosition != count($items) - 1) {
                $items[$swapItemIndex]->setSortposition($oldSortPosition);
                $items[$itemIndex]->setSortposition($newSortPosition);
            }
        }

        $em->flush();

        $response = new Response();
        $response->setStatusCode(200);

        return $response;
    }

    /**
     * @Route("/delete_section/{cv_id}/{section_id}", name="_cv_delete_section")
     */
    public function deleteSectionAction($cv_id, $section_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $this->getCv($cv_id);

        $section = $em->getRepository('CvCreatorBundle:CvSection')->find(array('cv' => $cv, 'id' => $section_id));

        $em->remove($section);
        $em->flush();

        $response = new Response();
        $response->setStatusCode(200);

        return $response;
    }
}
