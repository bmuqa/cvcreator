<?php

/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-08 17:11
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CvCreator\CvCreatorBundle\Form\Type;

/**
 * Class description:
 *
 * @Route("/")
 * @author Burim
 */
class DefaultController extends Controller
{
    /**
     * @Route("", name="_home")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('CvCreatorBundle:Default:index.html.twig', array('messages' => array()));
    }

    /**
     * @Route("disclaimer", name="_disclaimer")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function disclaimerAction()
    {
        return $this->render('CvCreatorBundle:Default:disclaimer.html.twig', array('messages' => array()));
    }

    /**
     * @Route("help", name="_help")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function helpAction()
    {
        return $this->render('CvCreatorBundle:Default:help.html.twig', array('messages' => array()));
    }

    /**
     * @Route("contact", name="_contact")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $result = false;
        $contactForm = $this->createForm(new Type\ContactType());

        if ($request->isMethod('POST')) {
            $contactForm->bind($request);

            if ($contactForm->isValid()) {

                $data = $contactForm->getData();

                $htmlEmail = $this->renderView(
                    'CvCreatorBundle:Email:contact.html.twig',
                    array(
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'message' => $data['message'],
                    )
                );

                $txtEmail = $this->renderView(
                    'CvCreatorBundle:Email:contact.txt.twig',
                    array(
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'message' => $data['message'],
                    )
                );

                // send contact
                $message = \Swift_Message::newInstance()
                    ->setSubject($this->get('translator')->trans('Kontakt aus ') . $this->container->getParameter('base_domain'))
                    ->setFrom($data['email'])
                    ->setTo($this->container->getParameter('mailer_sender_address'))
                    ->addPart($htmlEmail, 'text/html')
                    ->addPart($txtEmail, 'text/plain');

                $this->get('mailer')->send($message);

                $result = true;
            }
        }

        return $this->render(
            'CvCreatorBundle:Default:contact.html.twig',
            array(
                'form'         => $contactForm->createView(),
                'sendResult'   => $result,
            )
        );
    }

    /**
     * @Route("/sitemap.{_format}", name="_sitemap", Requirements={"_format" = "xml"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sitemapAction()
    {
        $urls = array();
        $hostname = $this->getRequest()->getHost();

        // default urls
        $urls[] = array('loc' => $this->get('router')->generate('_home'), 'changefreq' => 'weekly', 'priority' => '1.0');
        $urls[] = array('loc' => $this->get('router')->generate('_contact'), 'changefreq' => 'weekly', 'priority' => '1.0');
        $urls[] = array('loc' => $this->get('router')->generate('_disclaimer'), 'changefreq' => 'weekly', 'priority' => '1.0');

        // user urls
        $urls[] = array('loc' => $this->get('router')->generate('_user_login'), 'changefreq' => 'weekly', 'priority' => '1.0');
        $urls[] = array('loc' => $this->get('router')->generate('_user_sign'), 'changefreq' => 'weekly', 'priority' => '1.0');

        return $this->render(
            'CvCreatorBundle:Default:sitemap.xml.twig',
            array(
                'urls' => $urls,
                'hostname' => $hostname
            )
        );
    }
}
