<?php
/**
 * Created by JetBrains PhpStorm.
 * User: burim
 * Date: 20.02.13
 * Time: 20:45
 * To change this template use File | Settings | File Templates.
 */

namespace CvCreator\CvCreatorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

interface InitializableControllerInterface
{
    public function initialize(Request $request, SecurityContextInterface $security_context);
}
