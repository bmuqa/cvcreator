<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-16 17:28
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Form\Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class description:
 *
 * @Route("/user")
 * @author Burim
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="_user")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('_user_profile'));
    }

    /**
     * @Route("/profile", name="_user_profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction()
    {
        $status = '';
        $user = $this->get('security.context')->getToken()->getUser();
        $profileForm = $this->createForm(new Type\ProfileType(), $user);
        $changePasswordForm = $this->createForm(new Type\ChangePasswordType());

        $em = $this->getDoctrine()->getManager();

        if ($this->get('request')->getMethod() == 'POST') {
            $profileForm->bind($this->get('request'));

            if ($profileForm->isValid()) {

                /** @var $user2save Entity\User */
                $user2save = $profileForm->getData();

                if ($user->getId() != $user2save->getId()) {
                    $profileForm->addError(
                        new \Symfony\Component\Form\FormError(
                            $this->get('translator')->trans('Ein Fehler ist aufgetretten!')
                        )
                    );
                } else {
                    $status = 'profile_saved';

                    // todo: change the user firstname, lastname and email at the personal data
                    $cvs = $em->getRepository('CvCreatorBundle:Cv')->findByUser($user);
                    if (!empty($cvs)) {
                        foreach($cvs as $cv) {
                            /** @var $personalData Entity\CvPersonalData */
                            $personalData = $em->getRepository('CvCreatorBundle:CvPersonalData')->findOneByCv($cv);
                            $personalData->setFirstname($user2save->getFirstname());
                            $personalData->setLastname($user2save->getLastname());
                        }
                        $em->flush();
                    }
                }
            }

        }

        return $this->render(
            'CvCreatorBundle:User:profile.html.twig',
            array(
                'error'              => array(),
                'data'               => $user,
                'status'             => $status,
                'profileForm'        => $profileForm->createView(),
                'changePasswordForm' => $changePasswordForm->createView(),
            )
        );
    }

    /**
     * @Route("/change/password", name="_user_change_password")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction()
    {
        $status = '';
        $user = $this->get('security.context')->getToken()->getUser();
        $profileForm = $this->createForm(new Type\ProfileType(), $user);
        $changePasswordForm = $this->createForm(new Type\ChangePasswordType());

        $em = $this->getDoctrine()->getManager();

        if ($this->get('request')->getMethod() == 'POST') {
            $changePasswordForm->bind($this->get('request'));

            if ($changePasswordForm->isValid()) {

                $data = $changePasswordForm->getData();

                $encoderFactory = $this->get('security.encoder_factory');
                $encoder = $encoderFactory->getEncoder($user);
                $password = $encoder->encodePassword($data['password_old'], $user->getSalt());

                if ($user->getPassword() != $password) {
                    $changePasswordForm->get('password_old')->addError(
                        new \Symfony\Component\Form\FormError($this->get('translator')->trans('Das Alte Passwort stimmt nicht!'))
                    );
                } else {
                    $user->setPassword($password);
                    $em->flush();
                    $status = 'password_changed';
                }
            }

        }

        return $this->render(
            'CvCreatorBundle:User:profile.html.twig',
            array(
                'status'        => $status,
                'data'          => $user,
                'profileForm'   => $profileForm->createView(),
                'changePasswordForm'   => $changePasswordForm->createView(),
            )
        );
    }

    /**
     * @Route("/delete", name="_user_delete")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        /** @var $user Entity\User */
        $user = $this->get('security.context')->getToken()->getUser();

        if (!empty($user) && $user instanceof Entity\User) {
            $entityManager = $this->getDoctrine()->getManager();

            // remove user files
            $storagePath = realpath($this->container->getParameter('resources.storage.path'));
            $user->removeAllFiles($storagePath);

            // logout user
            $this->get('security.context')->setToken(null);
            $this->get('request')->getSession()->invalidate();

            // delete user
            $entityManager->remove($user);
            $entityManager->flush();
        } else {
            return $this->render(
                'CvCreatorBundle:Default:error.html.twig',
                array(
                    'errorMessage' => $this->get('translator')->trans("Es wurden keine Daten für diese Aktion gefunden!"),
                )
            );
        }

        return $this->render(
            'CvCreatorBundle:User:profile_deleted.html.twig',
            array(
                'error' => array(),
            )
        );
    }

    /**
     * @Route("/login", name="_user_login")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $translator = $this->get('translator');
        $error = null;
        $errorMessage = '';

        if ($request->query->get('login') == 'failure') {
            $errorMessage = $translator->trans('Du hast einen falschen Benutzernamen oder ein falsches Passwort eingegeben!');
            $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        $loginForm = $this->createForm(new Type\LoginType());
        $signForm = $this->createForm(new Type\RegistrationType(), new Entity\User());

        return $this->render(
            'CvCreatorBundle:User:login.html.twig',
            array(
                'last_username'     => $lastUsername,
                'errors'            => array(),
                'login_error'       => $errorMessage,
                'loginForm'         => $loginForm->createView(),
                'signForm'          => $signForm->createView(),
            )
        );
    }

    /**
     * @Route("/sign", name="_user_sign")
     */
    public function signAction()
    {
        $errors = null;
        $translator = $this->get('translator');
        $session = $this->getRequest()->getSession();
        $form = $this->createForm(new Type\RegistrationType(), new Entity\User());

        if ($this->getRequest()->getMethod() == 'POST') {

            $form->bind($this->getRequest());

            if ($form->isValid()) {

                /** @var $user Entity\User */
                $user = $form->getData();
                $entityManager = $this->getDoctrine()->getManager();

                $checkUser = $entityManager->getRepository('CvCreatorBundle:User')->findOneByEmail($user->getEmail());

                if ($checkUser instanceof Entity\User) {
                    $form->addError(
                        new \Symfony\Component\Form\FormError(
                            $translator->trans('Diese E-Mail-Adresse ist schon registriert!')
                        )
                    );
                    $errors = $form->getErrors();
                } else {
                    $user->setSalt(md5(time()));

                    $encoderFactory = $this->get('security.encoder_factory');
                    $encoder = $encoderFactory->getEncoder($user);
                    $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                    $user->setPassword($password);
                    $user->setUsername($user->getEmail());
                    $user->setConfirmcode($user->getSalt());
                    $user->setIsActive(false);

                    $entityManager->persist($user);
                    $entityManager->flush();

                    // last username entered by the user
                    $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

                    // send confirm E-Mail
                    $htmlEmail = $this->renderView(
                        'CvCreatorBundle:User:register_confirm_email.html.twig',
                        array(
                            'user_id' => $user->getId(),
                            'name' => $user->getFirstname(),
                            'email' => $user->getEmail(),
                            'confirm_code' => $user->getConfirmcode()
                        )
                    );

                    $txtEmail = $this->renderView(
                        'CvCreatorBundle:User:register_confirm_email.txt.twig',
                        array(
                            'user_id' => $user->getId(),
                            'name' => $user->getFirstname(),
                            'email' => $user->getEmail(),
                            'confirm_code' => $user->getConfirmcode()
                        )
                    );

                    // send contact
                    $message = \Swift_Message::newInstance()
                        ->setSubject($translator->trans('Registrierung auf ' . $this->container->getParameter('base_domain')))
                        ->setFrom($this->container->getParameter('mailer_sender_address'))
                        ->setTo($user->getEmail())
                        ->addPart($htmlEmail, 'text/html')
                        ->addPart($txtEmail, 'text/plain');

                    $this->get('mailer')->send($message);

                    // show output
                    return $this->render(
                        'CvCreatorBundle:User:register_success.html.twig',
                        array(
                            'last_username' => $lastUsername,
                            'message'       => $translator->trans('Sie sind erfolgreich registriert.'
                                . 'Bitte überprüfen Sie ihren E-Mail Postfach und aktivieren Sie ihre '
                                . 'Registrierung mit dem Bestätigungslink das ihnen '
                                . 'per E-Mail versendet wird.')
                        )
                    );
                }
            } else {

                $errors = array();
                foreach ($form->all() as $child) {
                    if ($child->hasErrors()) {
                        foreach($child->getErrors() as $error) {
                            $errors[] = $error->getMessageTemplate();
                        }
                    }
                }

            }
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        return $this->render(
            'CvCreatorBundle:User:sign.html.twig',
            array(
                'last_username' => $lastUsername,
                'errors'        => $errors,
                'signForm'      => $form->createView(),
            )
        );
    }


    /**
     * @Route("/sign/confirm/{user_id}/{confirm_code}", name="_sign_confirm")
     */
    public function signConfirmAction($user_id, $confirm_code)
    {
        $translator = $this->get('translator');
        $entityManager = $this->getDoctrine()->getManager();
        /** @var $user Entity\User */
        $user = $entityManager->getRepository('CvCreatorBundle:User')->find($user_id);

        if ($user instanceof Entity\User && $user->getConfirmcode() == $confirm_code) {
            $user->setIsActive(true);
            $entityManager->flush();

            return $this->render(
                'CvCreatorBundle:User:sign_confirm.html.twig',
                array()
            );
        } else {
            return $this->render(
                'CvCreatorBundle:Default:error.html.twig',
                array(
                    'errorMessage' => $translator->trans('Ein Fehler ist aufgetretten.'),
                )
            );
        }

    }

    /**
     * @Route("/reset/password", name="_reset_password")
     */
    public function resetPasswordAction(Request $request)
    {
        $status = "";
        $resetForm = $this->createForm(new Type\ResetPasswordType());

        if ($request->getMethod() == 'POST') {
            $resetForm->bind($request);

            if ($resetForm->isValid()) {

                // data is an array with "name", "email", and "message" keys
                $data = $resetForm->getData();

                $em = $this->getDoctrine()->getManager();

                /** @var $user Entity\User */
                $user = $em->getRepository('CvCreatorBundle:User')->findOneByEmail($data->getEmail());

                if (!empty($user) && $user instanceof Entity\User) {

                    $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                    $passwordResetCode = $encoder->encodePassword($user->getEmail() . time(), $user->getSalt());
                    $user->setResetpasswordcode($passwordResetCode);
                    $em->flush();

                    $htmlEmail = $this->renderView(
                        'CvCreatorBundle:Email:reset_password.html.twig',
                        array(
                            'name' => $user->getFirstname() . " " . $user->getLastname(),
                            'username' => $user->getUsername(),
                            'user_id' => $user->getId(),
                            'reset_code' => $user->getResetpasswordcode(),
                        )
                    );

                    $txtEmail = $this->renderView(
                        'CvCreatorBundle:Email:reset_password.txt.twig',
                        array(
                            'name' => $user->getFirstname() . " " . $user->getLastname(),
                            'username' => $user->getUsername(),
                            'user_id' => $user->getId(),
                            'reset_code' => $user->getResetpasswordcode(),
                        )
                    );

                    // send contact
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Passwort-Anfrage aus ' . $this->container->getParameter('mailer_user'))
                        ->setFrom($this->container->getParameter('mailer_sender_address'))
                        ->setTo($user->getEmail())
                        ->addPart($htmlEmail, 'text/html')
                        ->addPart($txtEmail, 'text/plain');

                    try {
                        $this->get('mailer')->send($message);
                        $status = "success";
                    } catch (\Exception $e) {
                        $status = "sending_error";
                    }
                } else {
                    $status = "not_found";
                }
            }
        }

        return $this->render(
            'CvCreatorBundle:User:reset_password.html.twig',
            array(
                'resetForm' => $resetForm->createView(),
                'status' => $status
            )
        );
    }

    /**
     * @Route("/new/password/{user_id}/{password_reset_code}", name="_new_password")
     */
    public function newPasswordAction($user_id, $password_reset_code)
    {
        $status = "error";
        $em = $this->getDoctrine()->getManager();
        $newPasswordForm = $this->createForm(new Type\NewPasswordType());

        /** @var $user Entity\User */
        $user = $em->getRepository('CvCreatorBundle:User')->find($user_id);

        if (!empty($user) && $user instanceof Entity\User && $user->getResetpasswordcode() == $password_reset_code) {

            if ($this->get('request')->getMethod() == 'POST') {
                $newPasswordForm->bind($this->get('request'));

                if ($newPasswordForm->isValid()) {

                    $data = $newPasswordForm->getData();

                    $encoderFactory = $this->get('security.encoder_factory');
                    $encoder = $encoderFactory->getEncoder($user);
                    $password = $encoder->encodePassword($data['password'], $user->getSalt());
                    $user->setResetpasswordcode('');
                    $user->setPassword($password);
                    $em->flush();

                    $status = 'success';
                }

            }

        } else {
            throw new NotFoundHttpException("Page not found");
        }

        return $this->render(
            'CvCreatorBundle:User:new_password.html.twig',
            array(
                'newPasswordForm' => $newPasswordForm->createView(),
                'user' => $user,
                'status' => $status,
                'user_id' => $user_id,
                'password_reset_code' => $password_reset_code
            )
        );
    }
}
