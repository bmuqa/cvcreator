<?php
 /**
 * This file is part of the cvcreator package.
 *
 * Created by: burim on 2013-03-16 14:16
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CvCreator\CvCreatorBundle\Entity\Applications
 *
 * @ORM\Table(name="applications")
 * @ORM\Entity
 */
class Application {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Cv $cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id", nullable=false)
     * })
     */
    protected $cv;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     */
    protected $user;

    /**
     * @var string $receiver_email
     *
     * @ORM\Column(name="receiver_email", type="string", nullable=false)
     */
    protected $receiver_email;

    /**
     * @var string $receiver_name
     *
     * @ORM\Column(name="receiver_name", type="string", nullable=false)
     */
    protected $receiver_name;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="string", nullable=false)
     */
    protected $subject;

    /**
     * @var string $message
     *
     * @ORM\Column(name="message", type="string", nullable=false)
     */
    protected $message;

    /**
     * @var integer $date
     *
     * @ORM\Column(name="date", type="bigint", nullable=true)
     */
    protected $date;

    /**
     * @var string $cv_content
     *
     * @ORM\Column(name="cv_content", type="string", nullable=true)
     */
    protected $cv_content;

    /**
     * @param \CvCreator\CvCreatorBundle\Entity\Cv $cv
     * @return Application
     */
    public function setCv($cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * @return \CvCreator\CvCreatorBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param int $date
     * @return Application
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $id
     * @return Application
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $message
     * @return Application
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $receiver_email
     * @return Application
     */
    public function setReceiverEmail($receiver_email)
    {
        $this->receiver_email = $receiver_email;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverEmail()
    {
        return $this->receiver_email;
    }

    /**
     * @param string $receiver_name
     * @return Application
     */
    public function setReceiverName($receiver_name)
    {
        $this->receiver_name = $receiver_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverName()
    {
        return $this->receiver_name;
    }

    /**
     * @param string $subject
     * @return Application
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param \CvCreator\CvCreatorBundle\Entity\User $user
     * @return Application
     */
    public function setUser(\CvCreator\CvCreatorBundle\Entity\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \CvCreator\CvCreatorBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $cv_content
     * @return Application
     */
    public function setCvContent($cv_content)
    {
        $this->cv_content = $cv_content;
        return $this;
    }

    /**
     * @return string
     */
    public function getCvContent()
    {
        return $this->cv_content;
    }


}