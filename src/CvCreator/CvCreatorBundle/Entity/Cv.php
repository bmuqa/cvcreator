<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CvCreator\CvCreatorBundle\Entity\Cv
 *
 * @ORM\Table(name="user_cvs")
 * @ORM\Entity
 */
class Cv
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer $createdate
     *
     * @ORM\Column(name="createDate", type="bigint", nullable=true)
     */
    protected $createdate;

    /**
     * @var integer $updatedate
     *
     * @ORM\Column(name="updateDate", type="bigint", nullable=true)
     */
    protected $updatedate;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var integer $format
     *
     * @ORM\Column(name="format", type="integer", nullable=true)
     */
    protected $format;

    /**
     * @var integer $layout
     *
     * @ORM\Column(name="layout", type="integer", nullable=true)
     */
    protected $layout;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     */
    protected $user;

    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * Set createdate
     *
     * @param integer $createdate
     * @return Cv
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;
        return $this;
    }

    /**
     * Get createdate
     *
     * @return integer 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set updatedate
     *
     * @param integer $updatedate
     * @return Cv
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;
        return $this;
    }

    /**
     * Get updatedate
     *
     * @return integer 
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Cv
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set format
     *
     * @param integer $format
     * @return Cv
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * Get format
     *
     * @return integer 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set layout
     *
     * @param integer $layout
     * @return Cv
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * Get layout
     *
     * @return integer 
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set user
     *
     * @param \CvCreator\CvCreatorBundle\Entity\User $user
     * @return Cv
     */
    public function setUser(\CvCreator\CvCreatorBundle\Entity\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \CvCreator\CvCreatorBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}