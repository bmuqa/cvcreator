<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * CvCreator\CvCreatorBundle\Entity\CvAttachment
 *
 * @ORM\Table(name="cv_attachments")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class CvAttachment implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    protected $type;

    /**
     * @var integer $uploaddate
     *
     * @ORM\Column(name="uploadDate", type="bigint", nullable=true)
     */
    protected $uploaddate;

    /**
     * @var string $uploadName
     *
     * @ORM\Column(name="uploadName", type="string", length=255, nullable=true)
     */
    protected $uploadName;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id", nullable=false)
     * })
     */
    protected $cv;

    /**
     * @var UploadedFile
     *
     * @Assert\File(maxSize="6000000")
     */
    public $file;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param $id
     * @return CvAttachment
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return CvAttachment
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set uploaddate
     *
     * @param integer $uploaddate
     * @return CvAttachment
     */
    public function setUploaddate($uploaddate)
    {
        $this->uploaddate = $uploaddate;
        return $this;
    }

    /**
     * Get uploaddate
     *
     * @return integer 
     */
    public function getUploaddate()
    {
        return !empty($this->uploaddate) ? $this->uploaddate : time();
    }

    /**
     * Set uploadName
     *
     * @param string $uploadName
     * @return CvAttachment
     */
    public function setUploadName($uploadName)
    {
        $this->uploadName = $uploadName;
        return $this;
    }

    /**
     * Get $uploadName
     *
     * @return string 
     */
    public function getUploadName()
    {
        return $this->uploadName;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvAttachment
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set cv
     *
     * @param Cv $cv
     * @return CvAttachment
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cv
     *
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        if (!$this->file instanceof UploadedFile && !empty($this->uploadName)) {
            $this->file = new UploadedFile($this->getAbsolutePath(), $this->getUploadName());
        }
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
        return $this;
    }

    /** File upload methods ********************/

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file && $this->file instanceof UploadedFile) {
            $filePath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->file->getClientOriginalName();
            $this->uploadName = substr($filePath, strrpos($filePath, DIRECTORY_SEPARATOR) + 1);
        } else {
            $this->file = null;
        }
        $this->uploaddate = time();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $targetPath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getStorageFileName();
        $this->file->move($this->getUploadRootDir(), $targetPath);

        $thumbTargetPath = substr($targetPath, 0, strrpos($targetPath, ".")) . '_thumb.png';

        // save thumb
        $this->getThumb(200, 200, $thumbTargetPath);
        $this->file = null;
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeUpload()
    {
        $filePath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getStorageFileName();
        $thumbFile = substr($filePath, 0, strrpos($filePath, ".")) . '_thumb.png';

        if (file_exists($filePath)) {
            @unlink($filePath);
        }

        if (file_exists($thumbFile)) {
            @unlink($thumbFile);
        }
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->uploadName
            ? null
            : realpath($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getStorageFileName());
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->uploadName
            ? null
            : $this->getUploadDir() . DIRECTORY_SEPARATOR . $this->getStorageFileName();
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__
            . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . 'Resources'
            . DIRECTORY_SEPARATOR . 'storage'
            . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    /**
     * @return null|string
     */
    protected function getUploadDir()
    {
        if ($this->cv instanceof Cv) {
            $userId = $this->cv->getUser()->getId();
            $cvId = $this->cv->getId();
            return $userId . DIRECTORY_SEPARATOR . $cvId . DIRECTORY_SEPARATOR . 'img';
        } else {
            return null;
        }
    }

    /*******************************************/

    /**
     * @param string $format
     * @return string
     */
    public function getMimeContentType($format = '')
    {
        if (empty($format)) {
            $fileName = $this->getFileName();
            $format = substr($fileName, strrpos($fileName, ".")+1);
        }

        switch ($format) {
            case 'pdf':
                $contentType = 'application/pdf; charset=utf-8';
                break;
            case 'rtf':
                $contentType = 'application/rtf';
                break;
            case 'doc':
                $contentType = 'application/msword';
                break;
            case 'jpg':
                $contentType = 'image/jpeg';
                break;
            case 'png':
                $contentType = 'image/png';
                break;
            case 'bmp':
                $contentType = 'image/bmp';
                break;
            default:
                $contentType = '';
        }

        return $contentType;
    }

    /**
     * @return null
     */
    public function getIcon()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        if (null === $this->uploadName) {
            return;
        }

        $pathExplode = explode(DIRECTORY_SEPARATOR, $this->uploadName);
        return $pathExplode[count($pathExplode)-1];
    }

    /**
     * @param int $width
     * @param int $height
     * @param null $targetFilePath
     * @return bool|null
     */
    protected function getThumb($width = 200, $height = 200, $targetFilePath = null)
    {
        $filePath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getStorageFileName();
        if (!file_exists($filePath)) {
            return null;
        }

        $extension = $this->getFileExtension();

        // Get Image size info
        list($widthOriginal, $heightOriginal) = @getimagesize($filePath);

        ini_set('memory_limit', '-1'); // we need unlimited memory

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $img = imagecreatefromjpeg($filePath);
                break;
            case 'png':
                $img = imagecreatefrompng($filePath);
                break;
            case 'gif':
                $img = imagecreatefromgif($filePath);
                break;
            case 'bmp':
                $img = $this->imagecreatefrombmp($filePath);
                break;
            case 'pdf':
                $extension = "png";
                $img = imagecreatefrompng($this->getIcon());
                list($widthOriginal, $heightOriginal) = getimagesize($this->getIcon());
                break;
            default:
                return false;
        }

        // Calculate the aspect ratio
        $aspectRatio = (float) $heightOriginal / $widthOriginal;
        $thumbWidth = $widthOriginal;

        // Calulate the thumbnail width based on the height
        $thumbHeight = round($thumbWidth * $aspectRatio);
        while ($thumbHeight > $height) {
            $thumbWidth -= 1;
            $thumbHeight = round($thumbWidth * $aspectRatio);
        }

        while ($thumbWidth > $width) {
            $thumbHeight -= 1;
            $thumbWidth = round($thumbHeight / $aspectRatio);
        }

        $newImg = imagecreatetruecolor($thumbWidth, $thumbHeight);

        // Set transparence for pngs
        if ($extension == "png") {
            imagealphablending($newImg, false);
            imagesavealpha($newImg, true);
            $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
            imagefilledrectangle($newImg, 0, 0, $thumbWidth, $thumbHeight, $transparent);
        }

        imagecopyresampled($newImg, $img, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $widthOriginal, $heightOriginal);
        if (!empty($filePath)) {
            imagepng($newImg, $targetFilePath);
        } else {
            imagepng($newImg);
        }
        imagedestroy($img);
        imagedestroy($newImg);
    }

    /**
     * @param $fileName
     */
    public function printHeaders($fileName)
    {
        Header('Content-type: ' . $this->getMimeContentType($fileName));
        Header('Content-Disposition: attachment; filename=' . $fileName);
        Header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        Header('Expires: 0');
        Header('Pragma: public');
    }

    /**
     * @return string
     */
    public function getFileExtension()
    {
        return substr($this->getFileName(), strrpos($this->getFileName(), ".") + 1);
    }

    /**
     * @return string
     */
    public function getStorageFileName()
    {
        return $this->getId() . '.' . $this->getFileExtension();
    }

    /**
     * @param bool $thumb
     * @return string
     */
    public function getFileContent($thumb = false)
    {
        $fileContent = '';
        $filePath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getStorageFileName();
        if ($thumb) {
            $thumbFilePath = substr($filePath, 0, strrpos($filePath, ".")) . '_thumb.png';
            if (file_exists($thumbFilePath)) {
                $filePath = $thumbFilePath;
            }
        }

        if (file_exists($filePath)) {
            $fileContent = file_get_contents($filePath);
        }

        return $fileContent;
    }
}