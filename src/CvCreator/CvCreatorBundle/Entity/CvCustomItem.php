<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\CvCustomItem
 *
 * @ORM\Table(name="cv_custom_items")
 * @ORM\Entity
 */
class CvCustomItem implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var CvSection
     *
     * @ORM\ManyToOne(targetEntity="CvSection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sectionId", referencedColumnName="id")
     * })
     */
    protected $section;

    /**
     * @var UserCv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CvCustomItem
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CvCustomItem
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvCustomItem
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set section
     *
     * @param \CvCreator\CvCreatorBundle\Entity\CvSection $section
     * @return CvCustomItem
     */
    public function setSection(\CvCreator\CvCreatorBundle\Entity\CvSection $section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Get section
     *
     * @return \CvCreator\CvCreatorBundle\Entity\CvSection
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set cv
     *
     * @param Cv $cv
     * @return CvSection
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    public function getCv()
    {
        return $this->cv;
    }
}