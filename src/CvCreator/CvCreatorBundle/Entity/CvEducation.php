<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\CvEducation
 *
 * @ORM\Table(name="cv_educations")
 * @ORM\Entity
 */
class CvEducation implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime $datefrom
     *
     * @ORM\Column(name="dateFrom", type="date", nullable=true)
     */
    protected $datefrom;

    /**
     * @var \DateTime $dateto
     *
     * @ORM\Column(name="dateTo", type="date", nullable=true)
     */
    protected $dateto;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string $level
     *
     * @ORM\Column(name="level", type="string", length=80, nullable=true)
     */
    protected $level;

    /**
     * @var string $school
     *
     * @ORM\Column(name="school", type="string", length=255, nullable=true)
     */
    protected $school;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var UserCv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;

    /**
     * Set setId
     *
     * @param integer $id
     * @return CvEducation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     * @return CvEducation
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set dateto
     *
     * @param \DateTime $dateto
     * @return CvEducation
     */
    public function setDateto($dateto)
    {
        $this->dateto = $dateto;
        return $this;
    }

    /**
     * Get dateto
     *
     * @return \DateTime
     */
    public function getDateto()
    {
        return $this->dateto;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CvEducation
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return CvEducation
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set school
     *
     * @param string $school
     * @return CvEducation
     */
    public function setSchool($school)
    {
        $this->school = $school;
        return $this;
    }

    /**
     * Get school
     *
     * @return string 
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvEducation
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set cv
     *
     * @param \CvCreator\CvCreatorBundle\Entity\Cv $cv
     * @return CvEducation
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cvId
     *
     * @return \CvCreator\CvCreatorBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}