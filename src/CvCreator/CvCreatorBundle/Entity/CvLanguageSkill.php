<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\CvLanguageSkill
 *
 * @ORM\Table(name="cv_language_skills")
 * @ORM\Entity
 */
class CvLanguageSkill implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $language
     *
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    protected $language;

    /**
     * @var string $rating
     *
     * @ORM\Column(name="rating", type="string", nullable=true)
     */
    protected $rating;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;

    /**
     * Set id
     *
     * @param $id
     * @return CvLanguageSkill
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return CvLanguageSkill
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set rating
     *
     * @param string $rating
     * @return CvLanguageSkill
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * Get rating
     *
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvLanguageSkill
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set cvId
     *
     * @param Cv $cv
     * @return CvLanguageSkill
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cvId
     *
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}