<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\User
 *
 * @ORM\Table(name="cv_personaldata")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class CvPersonalData implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $gender
     *
     * @ORM\Column(name="gender", type="string", nullable=false)
     */
    protected $gender;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    protected $firstname;

    /**
     * @var string $lastname
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * @var \DateTime $birthday
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    protected $birthday;

    /**
     * @var string $martialstatus
     *
     * @ORM\Column(name="martialStatus", type="string", nullable=true)
     */
    protected $martialstatus;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string $zip
     *
     * @ORM\Column(name="zip", type="string", length=10, nullable=true)
     */
    protected $zip;

    /**
     * @var string $city
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string $country
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    protected $country;

    /**
     * @var string $phone
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string $mobile
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    protected $mobile;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    protected $fax;

    /**
     * @var string $web
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=true)
     */
    protected $web;

    /**
     * @var string $addcontactdata
     *
     * @ORM\Column(name="addContactData", type="text", nullable=true)
     */
    protected $addcontactdata;

    /**
     * @var integer $createdate
     *
     * @ORM\Column(name="createDate", type="bigint", nullable=true)
     */
    protected $createdate;

    /**
     * @var integer $updatedate
     *
     * @ORM\Column(name="updateDate", type="bigint", nullable=true)
     */
    protected $updatedate;

    /**
     * @var CvAttachment $passphoto
     *
     * @ORM\ManyToOne(targetEntity="CvAttachment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="passphoto_id", referencedColumnName="id", nullable=true)
     * })
     */
    protected $passphoto;

    /**
     * @var integer $nationality
     *
     * @ORM\Column(name="nationality", type="string", nullable=true)
     */
    protected $nationality;


    /**
     * @var integer $secondNationality
     *
     * @ORM\Column(name="secondNationality", type="string", nullable=true)
     */
    protected $secondNationality;

    /**
     * @var string $driverLicense
     *
     * @ORM\Column(name="driverLicense", type="string", nullable=true)
     */
    protected $driverLicense;

    /**
     * @var string $noticePeriod
     *
     * @ORM\Column(name="noticePeriod", type="string", nullable=true)
     */
    protected $noticePeriod;

    /**
     * @var string $hobbies
     *
     * @ORM\Column(name="hobbies", type="text", nullable=true)
     */
    protected $hobbies;

    /**
     * @var UserCv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;

    /**
     * Set id
     *
     * @param integer $id
     * @return integer
     */
    public function setId($id)
    {
        return $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set birthday
     *
     * @param string $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * Get birthday
     *
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set martialstatus
     *
     * @param string $martialstatus
     * @return User
     */
    public function setMartialstatus($martialstatus)
    {
        $this->martialstatus = $martialstatus;
        return $this;
    }

    /**
     * Get martialstatus
     *
     * @return string
     */
    public function getMartialstatus()
    {
        return $this->martialstatus;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return User
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return User
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set web
     *
     * @param string $web
     * @return User
     */
    public function setWeb($web)
    {
        $this->web = $web;
        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set addcontactdata
     *
     * @param string $addcontactdata
     * @return User
     */
    public function setAddcontactdata($addcontactdata)
    {
        $this->addcontactdata = $addcontactdata;
        return $this;
    }

    /**
     * Get addcontactdata
     *
     * @return string
     */
    public function getAddcontactdata()
    {
        return $this->addcontactdata;
    }

    /**
     * Set createdate
     *
     * @param integer $createdate
     * @return User
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;
        return $this;
    }

    /**
     * Get createdate
     *
     * @return integer
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set updatedate
     *
     * @param integer $updatedate
     * @return User
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;
        return $this;
    }

    /**
     * Get updatedate
     *
     * @return integer
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set passphoto
     *
     * @param UploadedFile $passphoto
     * @return User
     */
    public function setPassphoto($passPhoto)
    {
        $this->passphoto = $passPhoto;
        return $this;
    }

    /**
     * Get passphoto
     *
     * @return CvAttachment
     */
    public function getPassphoto()
    {
        return $this->passphoto;
    }

    /**
     * Get nationality
     *
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set nationality
     *
     * @param string $nationality
     * @return User
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
        return $this;
    }

    /**
     * Get secondNationality
     *
     * @return integer
     */
    public function getSecondNationality()
    {
        return $this->secondNationality;
    }

    /**
     * Set secondNationality
     *
     * @param string $secondNationality
     * @return User
     */
    public function setSecondNationality($secondNationality)
    {
        $this->secondNationality = $secondNationality;
        return $this;
    }

    /**
     * Get driverLicense
     *
     * @return string
     */
    public function getDriverLicense()
    {
        return is_array($this->driverLicense) ? $this->driverLicense :
            (!empty($this->driverLicense) ? explode(',', $this->driverLicense) : array());
    }

    /**
     * Set driverLicense
     *
     * @param string $driverLicense
     * @return User
     */
    public function setDriverLicense($driverLicense)
    {
        $this->driverLicense = is_array($driverLicense) ? implode(',', $driverLicense) : $driverLicense;
        return $this;
    }

    /**
     * Get noticePeriod
     *
     * @return string
     */
    public function getNoticePeriod()
    {
        return $this->noticePeriod;
    }

    /**
     * Set noticePeriod
     *
     * @param string $noticePeriod
     * @return User
     */
    public function setNoticePeriod($noticePeriod)
    {
        $this->noticePeriod = $noticePeriod;
        return $this;
    }

    /**
     * Get hobbies
     *
     * @return string
     */
    public function getHobbies()
    {
        return $this->hobbies;
    }

    /**
     * Set hobbies
     *
     * @param string $hobbies
     * @return User
     */
    public function setHobbies($hobbies)
    {
        $this->hobbies = $hobbies;
        return $this;
    }

    public function getSortPosition()
    {
        return 1;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvPersonalData
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Set cv
     *
     * @param \CvCreator\CvCreatorBundle\Entity\Cv $cv
     * @return CvEducation
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cvId
     *
     * @return \CvCreator\CvCreatorBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * save the pass photo file into the database and filesystem
     */
    public function preparePassPhoto(
        \CvCreator\CvCreatorBundle\Entity\Cv $cv,
        \Doctrine\ORM\EntityManager $entityManager
    ) {
        $oldPassPhoto = null;
        $attachments = $entityManager->getRepository('CvCreatorBundle:CvAttachment')->findByCv($cv);
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                if ($attachment->getType() == 'passphoto') {
                    $oldPassPhoto = $attachment;
                }
            }
        }

        if ($this->passphoto instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            // delete all passphotos from this cv first
            if (!empty($oldPassPhoto)) {
                $entityManager->remove($attachment);
            }

            // Create a new attachment and save
            $attachment = new CvAttachment();
            $attachment->setCv($cv);
            $attachment->setType('passphoto');
            $attachment->setFile($this->passphoto);

            $entityManager->persist($attachment);

            $this->passphoto = $attachment;
        } else {
            if (!empty($oldPassPhoto)) {
                $this->passphoto = $oldPassPhoto;
            } else {
                // Create a new attachment and save
                $attachment = new CvAttachment();
                $attachment->setCv($cv);
                $attachment->setType('passphoto');
                $attachment->setUploadName('');

                $entityManager->persist($attachment);

                $this->passphoto = $attachment;
            }
        }
    }

}
