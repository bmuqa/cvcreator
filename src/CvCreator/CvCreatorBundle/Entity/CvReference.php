<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\CvReference
 *
 * @ORM\Table(name="cv_references")
 * @ORM\Entity
 */
class CvReference implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string $company
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    protected $company;

    /**
     * @var string $contactdata
     *
     * @ORM\Column(name="contactData", type="text", nullable=true)
     */
    protected $contactdata;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;

    /**
     * Set id
     *
     * @param $id
     * @return CvReference
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CvReference
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return CvReference
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contactdata
     *
     * @param string $contactdata
     * @return CvReference
     */
    public function setContactdata($contactdata)
    {
        $this->contactdata = $contactdata;
        return $this;
    }

    /**
     * Get contactdata
     *
     * @return string 
     */
    public function getContactdata()
    {
        return $this->contactdata;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvReference
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set cvId
     *
     * @param Cv $cv
     * @return CvReference
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cv
     *
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}