<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CvCreator\CvCreatorBundle\Entity\CvShare
 *
 * @ORM\Table(name="cv_shares")
 * @ORM\Entity
 */
class CvShare
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var integer $date
     *
     * @ORM\Column(name="date", type="bigint", nullable=true)
     */
    protected $date;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     */
    protected $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return CvShare
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set date
     *
     * @param integer $date
     * @return CvShare
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return integer 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set cv
     *
     * @param CvCreator\CvCreatorBundle\Entity\Cv $cv
     * @return CvShare
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv = null)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cv
     *
     * @return CvCreator\CvCreatorBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Set user
     *
     * @param CvCreator\CvCreatorBundle\Entity\User $user
     * @return CvShare
     */
    public function setUser(\CvCreator\CvCreatorBundle\Entity\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get userId
     *
     * @return CvCreator\CvCreatorBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}