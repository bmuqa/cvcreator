<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\CvWorkExperience
 *
 * @ORM\Table(name="cv_work_experiences")
 * @ORM\Entity
 */
class CvWorkExperience implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime $datefrom
     *
     * @ORM\Column(name="dateFrom", type="date", nullable=true)
     */
    protected $datefrom;

    /**
     * @var \DateTime $dateto
     *
     * @ORM\Column(name="dateTo", type="date", nullable=true)
     */
    protected $dateto;

    /**
     * @var boolean $stillworking
     *
     * @ORM\Column(name="stillWorking", type="boolean", nullable=true)
     */
    protected $stillworking;

    /**
     * @var string $profession
     *
     * @ORM\Column(name="profession", type="string", length=255, nullable=true)
     */
    protected $profession;

    /**
     * @var string $employment
     *
     * @ORM\Column(name="employment", type="string", nullable=true)
     */
    protected $employment;

    /**
     * @var string $company
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    protected $company;

    /**
     * @var string $activities
     *
     * @ORM\Column(name="activities", type="text", nullable=true)
     */
    protected $activities;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id")
     * })
     */
    protected $cv;

    /**
     * Set id
     *
     * @param $id
     * @return CvWorkExperience
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     * @return CvWorkExperience
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set dateto
     *
     * @param \DateTime $dateto
     * @return CvWorkExperience
     */
    public function setDateto($dateto)
    {
        $this->dateto = $dateto;
        return $this;
    }

    /**
     * Get dateto
     *
     * @return \DateTime
     */
    public function getDateto()
    {
        return $this->dateto;
    }

    /**
     * Set stillworking
     *
     * @param boolean $stillworking
     * @return CvWorkExperience
     */
    public function setStillworking($stillworking)
    {
        $this->stillworking = $stillworking;
        return $this;
    }

    /**
     * Get stillworking
     *
     * @return boolean 
     */
    public function getStillworking()
    {
        return $this->stillworking;
    }

    /**
     * Set profession
     *
     * @param string $profession
     * @return CvWorkExperience
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
        return $this;
    }

    /**
     * Get profession
     *
     * @return string 
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set employment
     *
     * @param string $employment
     * @return CvWorkExperience
     */
    public function setEmployment($employment)
    {
        $this->employment = $employment;
        return $this;
    }

    /**
     * Get employment
     *
     * @return string
     */
    public function getEmployment()
    {
        return $this->employment;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return CvWorkExperience
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set activities
     *
     * @param string $activities
     * @return CvWorkExperience
     */
    public function setActivities($activities)
    {
        $this->activities = $activities;
        return $this;
    }

    /**
     * Get activities
     *
     * @return string 
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvWorkExperience
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set cv
     *
     * @param Cv $cv
     * @return CvWorkExperience
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cv
     *
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}