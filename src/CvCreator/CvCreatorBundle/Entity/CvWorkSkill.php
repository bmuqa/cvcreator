<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CvCreator\CvCreatorBundle\Mapper\CvMapperItemInterface;

/**
 * CvCreator\CvCreatorBundle\Entity\CvWorkSkill
 *
 * @ORM\Table(name="cv_work_skills")
 * @ORM\Entity
 */
class CvWorkSkill implements CvMapperItemInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $skill
     *
     * @ORM\Column(name="skill", type="string", length=255, nullable=true)
     */
    protected $skill;

    /**
     * @var integer $rating
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    protected $rating;

    /**
     * @var integer $sortposition
     *
     * @ORM\Column(name="sortPosition", type="integer", nullable=true)
     */
    protected $sortposition;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cvId", referencedColumnName="id", nullable=false)
     * })
     */
    protected $cv;

    /**
     * Set id
     *
     * @param $id
     * @return CvWorkSkill
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set skill
     *
     * @param string $skill
     * @return CvWorkSkill
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;
        return $this;
    }

    /**
     * Get skill
     *
     * @return string 
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return CvWorkSkill
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set sortposition
     *
     * @param integer $sortposition
     * @return CvWorkSkill
     */
    public function setSortposition($sortposition)
    {
        $this->sortposition = $sortposition;
        return $this;
    }

    /**
     * Get sortposition
     *
     * @return integer 
     */
    public function getSortposition()
    {
        return $this->sortposition;
    }

    /**
     * Set cv
     *
     * @param Cv $cv
     * @return CvWorkSkill
     */
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * Get cv
     *
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}