<?php

namespace CvCreator\CvCreatorBundle\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * CvCreator\CvCreatorBundle\Entity\User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="CvCreator\CvCreatorBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements AdvancedUserInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $gender
     *
     * @ORM\Column(name="gender", type="string", nullable=false)
     */
    protected $gender;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    protected $firstname;

    /**
     * @var string $lastname
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * @var \DateTime $birthday
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    protected $birthday;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     *
     */
    protected $username;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     * @Assert\MinLength(limit=6, message="Das Passwort muss mindestens 6 Zeichen lang sein!")
     *
     */
    protected $password;

    /**
     * @var integer $createdate
     *
     * @ORM\Column(name="createDate", type="bigint", nullable=true)
     */
    protected $createdate;

    /**
     * @var integer $updatedate
     *
     * @ORM\Column(name="updateDate", type="bigint", nullable=true)
     */
    protected $updatedate;

    /**
     * @var boolean $isActive
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=false)
     */
    protected $isActive;

    /**
     * @var string $confirmcode
     *
     * @ORM\Column(name="confirmCode", type="string", length=255, nullable=true)
     */
    protected $confirmcode;

    /**
     * @var integer $loginattempt
     *
     * @ORM\Column(name="loginAttempt", type="integer", nullable=true)
     */
    protected $loginattempt;

    /**
     * @var string $resetpasswordcode
     *
     * @ORM\Column(name="resetPasswordCode", type="string", length=255, nullable=true)
     */
    protected $resetpasswordcode;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $salt;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return integer
     */
    public function setId($id)
    {
        return $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set birthday
     *
     * @param string $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * Get birthday
     *
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Set createdate
     *
     * @param integer $createdate
     * @return User
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;
        return $this;
    }

    /**
     * Get createdate
     *
     * @return integer 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set updatedate
     *
     * @param integer $updatedate
     * @return User
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;
        return $this;
    }

    /**
     * Get updatedate
     *
     * @return integer 
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set confirmcode
     *
     * @param string $confirmcode
     * @return User
     */
    public function setConfirmcode($confirmcode)
    {
        $this->confirmcode = $confirmcode;
        return $this;
    }

    /**
     * Get confirmcode
     *
     * @return string 
     */
    public function getConfirmcode()
    {
        return $this->confirmcode;
    }

    /**
     * Set loginattempt
     *
     * @param integer $loginattempt
     * @return User
     */
    public function setLoginattempt($loginattempt)
    {
        $this->loginattempt = $loginattempt;
        return $this;
    }

    /**
     * Get loginattempt
     *
     * @return integer 
     */
    public function getLoginattempt()
    {
        return $this->loginattempt;
    }

    /**
     * Set resetpasswordcode
     *
     * @param string $resetpasswordcode
     * @return User
     */
    public function setResetpasswordcode($resetpasswordcode)
    {
        $this->resetpasswordcode = $resetpasswordcode;
        return $this;
    }

    /**
     * Get resetpasswordcode
     *
     * @return string
     */
    public function getResetpasswordcode()
    {
        return $this->resetpasswordcode;
    }

    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
         return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
    }

    public function equals(AdvancedUserInterface $user)
    {
        return $this->username === $user->getUsername();
    }

    public function serialize()
    {
        return serialize(
            array(
                $this->id,
                $this->password,
                $this->username
            )
        );
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->password,
            $this->username
            ) = unserialize($serialized);
    }


    /**
     * @inheritdoc
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    public function getSortPosition()
    {
        return 1;
    }

    public function getCv()
    {
        return true; //$this->cv;
    }

    /**
     * Set cv
     *
     * @param Cv $cv
     * @return User
     */
    public function setCv(Cv $cv)
    {
        //$this->cv = $cv;
        return $this;
    }

    /**
     * Removes all user files and directories
     *
     * @param string $storagePath the storage path for user files
     * @param string $fullPath if this parameter is set storage path is not needed
     * @return bool
     */
    public function removeAllFiles($storagePath, $fullPath = '') {
        if (empty($storagePath) || !is_dir($storagePath)) {
            return false;
        }

        $userDir = empty($fullPath) ? $storagePath . DIRECTORY_SEPARATOR . $this->getId() : $fullPath;

        if (is_dir($userDir)) {
            $handle = opendir($userDir);
            while(false !== ($file = readdir($handle))) {
                if($file != '.' and $file != '..' ) {
                    $fullPath = $userDir . DIRECTORY_SEPARATOR . $file;
                    if(is_dir($fullPath)) {
                        $this->removeAllFiles($storagePath, $fullPath);
                    } else {
                        @unlink($fullPath);
                    }
                }
            }
            closedir($handle);
            @rmdir($userDir);
        }

        return !is_dir($storagePath . DIRECTORY_SEPARATOR . $this->getId());
    }
}
