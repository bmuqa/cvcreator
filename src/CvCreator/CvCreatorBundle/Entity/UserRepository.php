<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-28 15:12
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        $user = $this->getEntityManager()
            ->createQuery('SELECT u FROM CvCreatorBundle:User u WHERE u.username = :username OR u.email = :username')
            ->setParameters(array('username' => $username))
            ->getOneOrNullResult();

        if (empty($user)) {
            throw new UsernameNotFoundException(
                sprintf(
                    'Unable to find an active user identified by "%s".',
                    $username
                )
            );
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'CvCreator\CvCreatorBundle\Entity\User';
    }
}