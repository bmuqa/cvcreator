<?php
/**
 * Created by JetBrains PhpStorm.
 * User: burim
 * Date: 20.02.13
 * Time: 20:44
 * To change this template use File | Settings | File Templates.
 */

namespace CvCreator\CvCreatorBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use CvCreator\CvCreatorBundle\Controller\InitializableControllerInterface;

/**
 * @author Matt Drollette <matt@drollette.com>
 */
class BeforeControllerListener
{
    private $security_context;

    public function __construct(SecurityContextInterface $security_context)
    {
        $this->security_context = $security_context;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            // not a object but a different kind of callable. Do nothing
            return;
        }

        $controllerObject = $controller[0];

        // skip initializing for exceptions
        if ($controllerObject instanceof ExceptionController) {
            return;
        }

        if ($controllerObject instanceof InitializableControllerInterface) {
            // this method is the one that is part of the interface.
            $controllerObject->initialize($event->getRequest(), $this->security_context);
        }
    }
}
