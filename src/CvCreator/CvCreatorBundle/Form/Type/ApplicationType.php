<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2013-03-16 14:05
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ApplicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('user', 'hidden', array('data_class' => null, 'property_path' => 'user.id'));
        $builder->add('receiver_email', 'email', array('label' => 'form.application.receiver_email.label'));
        $builder->add('receiver_name', 'text', array('label' => 'form.application.receiver_name.label'));
        $builder->add('subject', 'text', array('label' => 'form.default.subject.label'));
        $builder->add(
            'message',
            'textarea',
            array(
                'label' => 'form.contact.message.label',
                'attr' => array('style' => 'width:400px', 'rows' => '10')
            )
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\Application'
        );
    }

    public function getName()
    {
        return 'application';
    }
}