<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:05
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class AttachmentType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add('type', 'choice', array('choices' => $this->getAttachmentTypes()));
        $builder->add(
            'file',
            'file',
            array(
                'label' => 'form.attachment.file.label',
                'required' => true,
                'attr' => array('accept' => 'image/png, image/jpeg', 'max-size' => 4194304)
            )
        );
    }

    public function getAttachmentTypes()
    {
        return array(
            'betweenCertificate'    => 'form.attachment.type.option.betweenCertificate',
            'workCertificate'       => 'form.attachment.type.option.workCertificate',
            'schoolCertificate'     => 'form.attachment.type.option.schoolCertificate',
            'educationCertificate'  => 'form.attachment.type.option.educationCertificate',
            'competenceCertificate' => 'form.attachment.type.option.competenceCertificate',
            'otherCertificate'      => 'form.attachment.type.option.otherCertificate',
            'diploma'               => 'form.attachment.type.option.diploma',
            'award'                 => 'form.attachment.type.option.award'
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvAttachment'
        );
    }

    public function getName()
    {
        return 'attachment';
    }
}
