<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:07
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * Class description:
 *
 * @author Burim
 */
class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('password_old', 'password', array('label' => 'form.changepassword.password_old.label'));

        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'form.changepassword.password.invalid_message',
            'first_options' => array('label' => 'form.changepassword.password.first_options.label'),
            'second_options' => array('label' => 'form.changepassword.password.second_options.label')
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }

    public function getName()
    {
        return 'changepassword';
    }
}
