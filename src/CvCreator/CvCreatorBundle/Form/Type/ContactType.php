<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:05
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True as Recaptcha;

/**
 * Class description:
 *
 * @author Burim
 */
class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label' => 'form.default.name.label'));
        $builder->add('email', 'email', array('label' => 'form.default.email.label'));
        $builder->add(
            'message',
            'textarea',
            array(
                'label' => 'form.contact.message.label',
                'attr' => array('style' => 'width:400px', 'rows' => '10')
            )
        );

        $builder->add('recaptcha', 'ewz_recaptcha', array(
            'label' => 'form.default.captcha.label',
            'attr' => array('options' => array(
                'theme' => 'clean',
                'lang' => 'de',
            )),
            'constraints' => new Recaptcha(array('message' => 'Der Sicherheitscode ist nicht gültig!')),
            'error_bubbling' => true
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }

    public function getName()
    {
        return 'contact';
    }
}
