<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:06
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class CustomSectionItemType extends AbstractType  implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('section', 'hidden', array('data_class' => null, 'property_path' => 'section.id'));
        $builder->add('sortPosition', 'hidden');
        $builder->add('title', 'text', array('label' => 'form.customsectionitem.title.label'));
        $builder->add('description', 'textarea', array(
                'label' => 'form.customsectionitem.description.label',
                'required' => true,
                'attr' => array('cols' => '80', 'rows' => '5')
            )
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvCustomItem'
        );
    }

    public function getName()
    {
        return 'customsectionitem';
    }
}
