<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-08 22:04
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Interface description:
 *
 * @author Burim
 */
interface CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options);
}
