<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:06
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class EducationType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add('dateFrom', 'date', array(
                'label' => 'form.default.dateFrom.label',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
            )
        );
        $builder->add('dateTo', 'date', array(
                'label' => 'form.default.dateTo.label',
                'format' => 'dd.MM.yyyy',
                'widget' => 'single_text',
                'required' => false
            )
        );
        $builder->add('title', 'text');

        $educations = array(
            'isced0' => 'from.education.level.option.isced0',
            'isced1' => 'from.education.level.option.isced1',
            'isced2' => 'from.education.level.option.isced2',
            'isced3' => 'from.education.level.option.isced3',
            'isced4' => 'from.education.level.option.isced4',
            'isced5' => 'from.education.level.option.isced5',
            'isced6' => 'from.education.level.option.isced6',
            'other' => 'from.education.level.option.other',
        );

        $builder->add('level', 'choice', array('choices' => $educations, 'label' => 'form.education.level.label'));
        $builder->add('school', 'text', array('label' => 'form.education.school.label', 'required' => false));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvEducation'
        );
    }

    public function getName()
    {
        return 'education';
    }
}

