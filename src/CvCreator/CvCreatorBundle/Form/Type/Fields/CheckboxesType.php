<?php

namespace CvCreator\CvCreatorBundle\Form\Type\Fields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CheckboxesType extends AbstractType
{
    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'checkboxes';
    }
}