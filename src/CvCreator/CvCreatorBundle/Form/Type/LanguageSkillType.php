<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:07
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class LanguageSkillType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add('language', 'text', array('label' => 'form.languageskill.language.label'));
        $builder->add('rating', 'choice', array(
                'choices' => $this->getLanguageLevels(),
                'label' => 'form.languageskill.rating.label'
            )
        );
    }

    public function getLanguageLevels()
    {
        return array(
            'A1' => 'form.languageskill.rating.option.a1',
            'A2' => 'form.languageskill.rating.option.a2',
            'B1' => 'form.languageskill.rating.option.b1',
            'B2' => 'form.languageskill.rating.option.b2',
            'C1' => 'form.languageskill.rating.option.c1',
            'C2' => 'form.languageskill.rating.option.c2',
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvLanguageSkill'
        );
    }

    public function getName()
    {
        return 'languageskill';
    }
}
