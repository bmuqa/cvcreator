<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-17 20:16
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('_username', 'text', array('label' => 'form.default.username.label', 'required' => true));
        $builder->add('_password', 'password', array('label' => 'form.default.password.label', 'required' => true));
        $builder->add('remember_me', 'checkbox', array('label' => 'form.login.remember_me.label', 'required' => false));
    }

    public function getName()
    {
        return 'login';
    }
}
