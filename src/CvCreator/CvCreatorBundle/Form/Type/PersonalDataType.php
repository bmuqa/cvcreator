<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-08 21:08
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class PersonalDataType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add(
            'gender',
            'choice',
            array(
                'choices'   => array(
                    'Male' => 'form.personaldata.gender.option.male',
                    'Female' => 'form.personaldata.gender.option.female'
                ),
                'required'  => false,
                'label' => 'form.personaldata.gender.label'
            )
        );

        $builder->add('firstname', 'text', array('label' => 'form.personaldata.firstname.label', 'disabled' => 'disabled'));
        $builder->add('lastname', 'text', array('label' => 'form.personaldata.lastname.label', 'disabled' => 'disabled'));
        $builder->add('email', 'text', array('label' => 'form.default.email.label', 'disabled' => 'disabled'));
        $builder->add('birthday', null, array(
                'label' => 'form.personaldata.birthday.label',
                'format' => 'dd.MM.yyyy',
                'widget' => 'single_text',
                'required' => false
            )
        );

        $builder->add(
            'martialStatus',
            'choice',
            array(
                'label' => 'form.personaldata.martial_status.label',
                'choices' => $this->getMartialStatusOptions(),
                'required' => false
            )
        );

        $builder->add(
            'driverLicense',
            new Fields\CheckboxesType(),
            array(
                'label' => 'form.personaldata.driver_license.label',
                'required' => false,
                'multiple'  => true,
                'expanded' => true,
                'choices' => $this->getDriverLicenseOptions(),
            )
        );

        $builder->add('address', 'text', array('label' => 'form.personaldata.address.label', 'required' => false));
        $builder->add('zip', 'text', array('label' => 'form.personaldata.zip.label', 'required' => false));
        $builder->add('city', 'text', array('label' => 'form.personaldata.city.label', 'required' => false));
        $builder->add('country', 'text', array('label' => 'form.personaldata.country.label', 'required' => false));
        $builder->add('phone', 'text', array('label' => 'form.personaldata.phone.label', 'required' => false));
        $builder->add('mobile', 'text', array('label' => 'form.personaldata.mobile.label', 'required' => false));
        $builder->add('fax', 'text', array('label' => 'form.personaldata.fax.label', 'required' => false));
        $builder->add('web', 'text', array('label' => 'form.personaldata.web.label', 'required' => false));
        $builder->add('addContactData', 'textarea', array('label' => 'form.personaldata.add_contact_data.label', 'required' => false));
        $builder->add('nationality', 'text', array('label' => 'form.personaldata.nationality.label', 'required' => false));
        $builder->add('secondNationality', 'text', array('label' => 'form.personaldata.second_nationality.label', 'required' => false));
        $builder->add('noticePeriod', 'text', array('label' => 'form.personaldata.notice_period.label', 'required' => false));
        $builder->add('hobbies', 'textarea', array(
                'label' => 'form.personaldata.hobbies.label',
                'required' => false,
                'attr' => array('cols' => '80', 'rows' => '5')
            )
        );
        $builder->add(
            'passphoto',
            'file',
            array(
                'label' => 'form.personaldata.passphoto.label',
                'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvAttachment',
                'required' => false,
                'attr' => array('accept' => 'image/png, image/jpeg', 'max-size' => 4194304)
            )
        );
    }

    public function getMartialStatusOptions()
    {
        return array(
            ''                  => 'form.personaldata.martial_status.option.empty',
            'DomesticPartner'   => 'form.personaldata.martial_status.option.domestic_partner',
            'Divorced'          => 'form.personaldata.martial_status.option.divorced',
            'Married'           => 'form.personaldata.martial_status.option.married',
            'Unreported'        => 'form.personaldata.martial_status.option.unreported',
            'Separated'         => 'form.personaldata.martial_status.option.separated',
            'Unmarried'         => 'form.personaldata.martial_status.option.unmarried',
            'Widowed'           => 'form.personaldata.martial_status.option.widowed',
            'LegallySeparated'  => 'form.personaldata.martial_status.option.legally_separated',
        );
    }

    public function getDriverLicenseOptions()
    {
        return array(
            'A' => 'form.personaldata.driver_license.option.a',
            'A1' => 'form.personaldata.driver_license.option.a1',
            'B' => 'form.personaldata.driver_license.option.b',
            'BE' => 'form.personaldata.driver_license.option.be',
            'B1' => 'form.personaldata.driver_license.option.b1',
            'C' => 'form.personaldata.driver_license.option.c',
            'C1E' => 'form.personaldata.driver_license.option.c1e',
            'D' => 'form.personaldata.driver_license.option.d',
            'DE' => 'form.personaldata.driver_license.option.de',
            'D1' => 'form.personaldata.driver_license.option.d1',
            'D1E' => 'form.personaldata.driver_license.option.d1e',
            'F' => 'form.personaldata.driver_license.option.f',
            'G' => 'form.personaldata.driver_license.option.g',
            'M' => 'form.personaldata.driver_license.option.m',
            'BPT' => 'form.personaldata.driver_license.option.bpt',
            'CZV' => 'form.personaldata.driver_license.option.czv',
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvPersonalData'
        );
    }

    public function getName()
    {
        return 'personaldata';
    }
}
