<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:08
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
        $builder->add(
            'gender',
            'choice',
            array(
                'choices'   => array(
                    'Male' => 'form.personaldata.gender.option.male',
                    'Female' => 'form.personaldata.gender.option.female'
                ),
                'required'  => false,
                'label' => 'form.personaldata.gender.label'
            )
        );

        $builder->add('firstname', 'text', array('label' => 'form.personaldata.firstname.label'));
        $builder->add('lastname', 'text', array('label' => 'form.personaldata.lastname.label'));
        $builder->add('birthday', null, array(
                'label' => 'form.personaldata.birthday.label',
                'format' => 'dd.MM.yyyy',
                'widget' => 'single_text')
        );
        $builder->add('email', 'text', array('label' => 'form.default.email.label', 'disabled' => 'disabled'));
        $builder->add('username', 'text', array('label' => 'form.default.username.label'));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\User'
        );
    }

    public function getName()
    {
        return 'profile';
    }
}
