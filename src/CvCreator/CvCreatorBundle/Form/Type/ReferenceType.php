<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:08
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class ReferenceType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add('name', 'text', array('label' => 'form.reference.name.label'));
        $builder->add('company', 'text', array('label' => 'form.reference.company.label', 'required' => false));
        $builder->add('contactData', 'textarea', array('label' => 'form.reference.contact_data.label', 'required' => false));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvReference'
        );
    }

    public function getName()
    {
        return 'reference';
    }
}
