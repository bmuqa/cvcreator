<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:08
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\True;
use Symfony\Component\Validator\Constraints\Collection;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True as Recaptcha;

/**
 * Class description:
 *
 * @author Burim
 */
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
        $builder->add(
            'gender',
            'choice',
            array(
                'choices'   => array(
                    'Male' => 'form.personaldata.gender.option.male',
                    'Female' => 'form.personaldata.gender.option.female'
                ),
                'required'  => false,
                'label' => 'form.personaldata.gender.label'
            )
        );

        $builder->add('firstname', 'text', array('label' => 'form.personaldata.firstname.label', 'error_bubbling' => true));
        $builder->add('lastname', 'text', array('label' => 'form.personaldata.lastname.label', 'error_bubbling' => true));
        $builder->add('email', 'text', array('label' => 'form.default.email.label'));
        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'form.changepassword.password.invalid_message',
            'first_options' => array('label' => 'form.registration.password.first_options.label'),
            'second_options' => array('label' => 'form.registration.password.second_option.label')
        ));

        $builder->add(
            'terms_and_conditions',
            'checkbox',
            array(
                'label' => 'form.registration.terms_and_conditions.label',
                'property_path' => false,
                'constraints' => new True(array('message' => 'form.registration.terms_and_conditions.error'))
            )
        );

//        $builder->add('recaptcha', 'ewz_recaptcha', array(
//            'label' => 'form.default.captcha.label',
//            'attr' => array('options' => array(
//                'theme' => 'clean',
//                'lang' => 'de',
//            )),
//            'constraints' => new Recaptcha(array('message' => 'Der Sicherheitscode ist nicht gültig!')),
//            'error_bubbling' => true,
//            'property_path' => false
//        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\User',
        );
    }

    public function getName()
    {
        return 'registration';
    }
}
