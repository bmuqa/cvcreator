<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:09
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True as Recaptcha;

/**
 * Class description:
 *
 * @author Burim
 */
class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email', array('label' => 'E-Mail'));

        $builder->add('recaptcha', 'ewz_recaptcha', array(
            'label' => 'form.default.captcha.label',
            'attr' => array('options' => array(
                'theme' => 'clean',
                'lang' => 'de',
            )),
            'constraints' => new Recaptcha(array('message' => 'Der Sicherheitscode ist nicht gültig!')),
            'error_bubbling' => true,
            'property_path' => false
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\User'
        );
    }

    public function getName()
    {
        return 'resetpassword';
    }
}
