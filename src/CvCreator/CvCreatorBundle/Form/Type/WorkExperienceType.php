<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:10
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class WorkExperienceType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add('dateFrom', 'date', array(
                'label' => 'form.default.dateFrom.label',
                'format' => 'dd.MM.yyyy',
                'widget' => 'single_text'
            )
        );
        $builder->add('dateTo', 'date', array(
                'label' => 'form.default.dateTo.label',
                'format' => 'dd.MM.yyyy',
                'widget' => 'single_text',
                'required' => false
            )
        );
        $builder->add('stillWorking', 'checkbox', array('label' => 'form.workexperience.still_working.label', 'required' => false));
        $builder->add('profession', 'text', array('label' => 'form.workexperience.profession.label'));

        $builder->add(
            'employment',
            'choice',
            array('label' => 'form.workexperience.employment.label', 'choices' => $this->getEmployments(), 'required' => false)
        );
        $builder->add('company', 'text', array('label' => 'form.workexperience.company.label', 'required' => false));
        $builder->add('activities', 'textarea', array('label' => 'form.workexperience.activities.label', 'required' => false));
    }

    public function getEmployments()
    {
        return array(
            'permanent' => 'form.workexperience.employment.option.permanent',
            'temporary' => 'form.workexperience.employment.option.temporary',
            'freelance' => 'form.workexperience.employment.option.freelance',
            'practica' => 'form.workexperience.employment.option.practica',
            'sideline' => 'form.workexperience.employment.option.sideline',
            'apprenticeship' => 'form.workexperience.employment.option.apprenticeship',
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvWorkExperience'
        );
    }

    public function getName()
    {
        return 'workexperience';
    }
}
