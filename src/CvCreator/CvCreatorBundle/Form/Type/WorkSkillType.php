<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-19 21:10
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class WorkSkillType extends AbstractType implements CvFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cv', 'hidden', array('data_class' => null, 'property_path' => 'cv.id'));
        $builder->add('id', 'hidden');
        $builder->add('sortPosition', 'hidden');
        $builder->add('skill', 'text', array('label' => 'form.workskill.skill.label'));

        $builder->add('rating', 'choice', array('label' => 'form.workskill.rating.label', 'choices' => $this->getRatings()));
    }

    public function getRatings()
    {
        return array(
            5 => 'form.workskill.rating.option.5',
            4 => 'form.workskill.rating.option.4',
            3 => 'form.workskill.rating.option.3',
            2 => 'form.workskill.rating.option.2',
            1 => 'form.workskill.rating.option.1',
        );
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CvCreator\CvCreatorBundle\Entity\CvWorkSkill'
        );
    }

    public function getName()
    {
        return 'workskill';
    }
}
