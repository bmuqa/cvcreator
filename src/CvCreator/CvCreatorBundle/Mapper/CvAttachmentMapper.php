<?php
 /**
 * This file is part of the cvcreator package.
 *
 * Created by: burim on 2013-03-17 13:11
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Mapper;


class CvAttachmentMapper extends CvSectionMapper
{
    public function initData(\Doctrine\Common\Persistence\ObjectManager $entityManager)
    {
        $data = $entityManager->getRepository('CvCreatorBundle:' . $this->getEntityName())
            ->findBy(array('cv' => $this->cv), array('sortposition' => 'ASC'));

        foreach ($data as $item) {
            if ($item->getType() != 'passphoto') {
                $this->addItem($item);
            }
        }
    }
}