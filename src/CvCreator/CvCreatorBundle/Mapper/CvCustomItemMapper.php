<?php
/**
 * This file is part of the cvcreator package.
 *
 * Created by: burim on 2013-03-17 13:11
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Mapper;

use CvCreator\CvCreatorBundle\Entity;

class CvCustomItemMapper extends CvSectionMapper
{
    /**
     * @param $entityManager
     */
    public function initData(\Doctrine\Common\Persistence\ObjectManager $entityManager)
    {
        $section = $entityManager->getRepository('CvCreatorBundle:CvSection')->find($this->getId());
        if ($section instanceof Entity\CvSection) {
            $data = $entityManager->getRepository('CvCreatorBundle:' . $this->getEntityName())
                ->findBy(array('cv' => $this->cv, 'section' => $section), array('sortposition' => 'ASC'));

            foreach ($data as $item) {
                $this->addItem($item);
            }
        }
    }
}