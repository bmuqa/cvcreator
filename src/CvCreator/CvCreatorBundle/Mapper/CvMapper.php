<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-06 23:56
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Mapper;

use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Form\Type;
use Symfony\Component\Form;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class description:
 *
 * @author Burim
 */
class CvMapper
{
    private $sections;
    private $cv;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ContainerInterface;
     */
    private $container;

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $entityManager
     * @param ContainerInterface $container
     * @param Entity\Cv $cv
     */
    public function __construct(
        \Doctrine\Common\Persistence\ObjectManager $entityManager,
        ContainerInterface $container,
        Entity\Cv $cv
    ) {
        $this->entityManager = $entityManager;
        $this->cv = $cv;
        $this->container = $container;
        $this->translator = $container->get('translator');
    }

    private function getSectionSettings()
    {
        return array(
            'personaldata' => array(
                'title' => $this->translator->trans('Persönliche Daten'),
                'movable' => false,
                'multiple' => false
            ),
            'workexperience' => array(
                'title' => $this->translator->trans('Berufserfahrung'),
                'movable' => true,
                'multiple' => true
            ),
            'education' => array(
                'title' => $this->translator->trans('Aus- & Weiterbildung'),
                'movable' => true,
                'multiple' => true
            ),
            'languageskill' => array(
                'title' => $this->translator->trans('Sprachkenntnisse'),
                'movable' => true,
                'multiple' => true
            ),
            'workskill' => array(
                'title' => $this->translator->trans('Berufliche Fähigkeiten / PC Kenntnisse'),
                'movable' => true,
                'multiple' => true
            ),
            'reference' => array(
                'title' => $this->translator->trans('Referenzen'),
                'movable' => true,
                'multiple' => true
            ),
            'customsectionitem' => array(
                'title' => $this->translator->trans('Benutzerdefinierter Bereich'),
                'movable' => true,
                'multiple' => true,
            ),
            'attachment' => array(
                'title' => $this->translator->trans('Anhang'),
                'movable' => false,
                'multiple' => true
            ),
        );
    }

    private function getCvSectionMapper(
        Type\CvFormTypeInterface $formType,
        Entity\CvSection $section,
        $movable,
        $multiple
    ) {
        $mapper = null;
        switch ($formType->getName()) {
            case 'personaldata':
                return new CvPersonalDataMapper($formType, $this->cv, $section, $movable, $multiple);
                break;
            case 'attachment':
                $mapper = new CvAttachmentMapper($formType, $this->cv, $section, $movable, $multiple);
                break;
            case 'customsectionitem':
                $mapper = new CvCustomItemMapper($formType, $this->cv, $section, $movable, $multiple);
                break;
            default:
                $mapper = new CvSectionMapper($formType, $this->cv, $section, $movable, $multiple);
        }

        return $mapper;
    }

    /**
     * Save default sections
     */
    public function saveInitialSections()
    {
        $sectionSettings = $this->getSectionSettings();
        $index = 0;

        /** @var CvSectionMapperInterface $section */
        foreach ($sectionSettings as $formType => $section) {
            $newSection = new Entity\CvSection();
            $newSection->setCv($this->cv);
            $newSection->setTitle($section['title']);
            $newSection->setSortposition($index);
            $newSection->setFormType($formType);

            $this->entityManager->persist($newSection);
            $index++;
        }

        $this->entityManager->flush();
    }

    /**
     * Init custom sections
     */
    private function initSections()
    {
        $sectionSettings = $this->getSectionSettings();
        $sections = $this->entityManager
            ->getRepository('CvCreatorBundle:CvSection')
            ->findBy(array('cv' => $this->cv), array('sortposition' => 'ASC'));

        if (empty($sections)) {
            $this->saveInitialSections();

            $sections = $this->entityManager
                ->getRepository('CvCreatorBundle:CvSection')
                ->findBy(array('cv' => $this->cv), array('sortposition' => 'ASC'));
        }

        if (!empty($sections)) {
            /** @var Entity\CvSection $section */
            foreach ($sections as $section) {
                /** @var Type\CvFormTypeInterface $formType */
                $formType = $this->container->get('cvcreator.type.' . $section->getFormType());
                $movable = $sectionSettings[$section->getFormType()]['movable'];
                $multiple = $sectionSettings[$section->getFormType()]['multiple'];
                $this->sections[] = $this->getCvSectionMapper($formType, $section, $movable, $multiple);
            }
        }
    }

    /**
     * @param string $formName
     * @param int $id
     * @return null|CvSection
     */
    public function getSection($formName, $id = 0)
    {
        $resultSection = null;
        if (empty($this->sections)) {
            $this->initSections();
        }

        foreach ($this->sections as $section) {
            if ($section->getFormName() == $formName && $section->getId() == $id) {
                $section->initData($this->entityManager);
                $resultSection = $section;
                break;
            }
        }

        return $resultSection;
    }

    /**
     * @return mixed
     */
    public function getSections()
    {
        if (empty($this->sections)) {
            $this->initSections();
        }

        if (!empty($this->cv)) {
            foreach ($this->sections as $section) {
                $section->initData($this->entityManager);
            }
        }

        return $this->sections;
    }
}
