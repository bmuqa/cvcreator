<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-06 22:59
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Mapper;

/**
 * Description:
 *
 * @author Burim
 */
interface  CvMapperItemInterface
{
    public function getId();
    public function getSortPosition();
    public function setSortPosition($sortPosition);
    public function getCv();
    public function setCv(\CvCreator\CvCreatorBundle\Entity\Cv $cv);
}
