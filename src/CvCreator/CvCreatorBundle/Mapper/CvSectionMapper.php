<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-06 22:57
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Mapper;

use CvCreator\CvCreatorBundle\Mapper\CvSectionMapperInterface;
use CvCreator\CvCreatorBundle\Form\Type\CvFormTypeInterface;
use CvCreator\CvCreatorBundle\Entity;
use Symfony\Component\Form;

/**
 * Class description:
 *
 * @author Burim
 */
class CvSectionMapper implements CvSectionMapperInterface
{
    /**
     * @var array
     */
    protected $items;

    /**
     * @var
     */
    protected $title;

    /**
     * @var \CvCreator\CvCreatorBundle\Entity\Cv
     */
    protected $cv;

    /**
     * @var \CvCreator\CvCreatorBundle\Form\Type\CvFormTypeInterface
     */
    protected $form;

    /**
     * @var
     */
    protected $formView;

    /**
     * @var \CvCreator\CvCreatorBundle\Entity\CvSection
     */
    protected $section;

    /**
     * @var
     */
    protected $movable;

    /**
     * @var
     */
    protected $multiple;

    /**
     * @param CvFormTypeInterface $form
     * @param Entity\Cv $cv
     * @param Entity\CvSection $section
     */
    public function __construct(
        CvFormTypeInterface $form,
        Entity\Cv $cv = null,
        Entity\CvSection $section,
        $movable,
        $multiple
    ) {
        $this->form = $form;
        $this->section = $section;
        $this->cv = $cv;
        $this->movable = $movable;
        $this->multiple = $multiple;
        $this->items = array();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->section->getId();
    }

    /**
     * @param CvMapperItemInterface $item
     */
    public function addItem(CvMapperItemInterface $item)
    {
        $this->items[$item->getId()] = $item;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $id
     * @return null
     */
    public function getItem($id)
    {
        return !empty($this->items[$id]) ? $this->items[$id] : null;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->section->getTitle();
    }

    /**
     * @return bool
     */
    public function isMovable()
    {
        return $this->movable;
    }

    /**
     * @return bool
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @return CvFormTypeInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param Form\FormView $formView
     */
    public function setFormView(Form\FormView $formView)
    {
        $this->formView = $formView;
    }

    /**
     * @return Form\FormView
     */
    public function getFormView()
    {
        if (!empty($this->formView) && $this->formView instanceof Form\FormView) {
            return $this->formView;
        }

        return new Form\FormView();
    }

    /**
     * @return CvFormTypeInterface
     */
    public function getType()
    {
        return $this->form;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        $options = $this->form->getDefaultOptions(array());
        $entity = new $options['data_class'];
        $entity->setCv($this->cv);
        return $entity;
    }

    /**
     * @return mixed
     */
    public function getEntityName()
    {
        $options = $this->form->getDefaultOptions(array());
        $entity = explode('\\', $options['data_class']);
        return array_pop($entity);
    }

    /**
     * @return mixed
     */
    public function getFormName()
    {
        return $this->form->getName();
    }

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $entityManager
     */
    public function initData(\Doctrine\Common\Persistence\ObjectManager $entityManager)
    {
        if ($this->getFormName() == 'personaldata') {
            $data = $entityManager->getRepository('CvCreatorBundle:' . $this->getEntityName())
                ->findBy(array('cv' => $this->cv));
        } else {
            $data = $entityManager->getRepository('CvCreatorBundle:' . $this->getEntityName())
                              ->findBy(array('cv' => $this->cv), array('sortposition' => 'ASC'));
        }
        foreach ($data as $item) {
            $this->addItem($item);
        }
    }
}
