<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-10-07 00:03
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace CvCreator\CvCreatorBundle\Mapper;

use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Form\Type\CvFormTypeInterface;
use Symfony\Component\Form;

/**
 * Interface description:
 *
 * @author Burim
 */
interface CvSectionMapperInterface
{
    public function __construct(
        CvFormTypeInterface $form,
        Entity\Cv $cv = null,
        Entity\CvSection $section,
        $isMovable,
        $isMultiple
    );
    public function getId();
    public function getItems();
    public function addItem(CvMapperItemInterface $item);
    public function getItem($id);
    public function getTitle();
    public function isMovable();
    public function isMultiple();
    public function getForm();
    public function setFormView(Form\FormView $formView);
    public function getFormView();
    public function getType();
    public function getEntity();
    public function getEntityName();
    public function getFormName();
    public function initData(\Doctrine\Common\Persistence\ObjectManager $entityManager);

}
