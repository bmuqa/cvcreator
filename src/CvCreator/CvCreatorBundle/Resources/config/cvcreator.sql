CREATE DATABASE  IF NOT EXISTS `cvcreator` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cvcreator`;
-- MySQL dump 10.13  Distrib 5.5.24, for osx10.5 (i386)
--
-- Host: 127.0.0.1    Database: cvcreator
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cv_attachments`
--

DROP TABLE IF EXISTS `cv_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `type` set('passphoto','workCertificate','betweenCertificate','schoolCertificate','educationCertificate','competenceCertificate','otherCertificate','diploma','award') DEFAULT NULL,
  `uploadDate` bigint(20) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`,`cvId`),
  KEY `FK_cv_attachments_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_attachments_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_attachments`
--

LOCK TABLES `cv_attachments` WRITE;
/*!40000 ALTER TABLE `cv_attachments` DISABLE KEYS */;
INSERT INTO `cv_attachments` VALUES (343,51,'workCertificate',NULL,'passfoto-14.png',1),(344,51,'passphoto',NULL,'passfoto-03.png',NULL);
/*!40000 ALTER TABLE `cv_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_custom_section_items`
--

DROP TABLE IF EXISTS `cv_custom_section_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_custom_section_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sectionId` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`,`sectionId`),
  KEY `FK_cv_custom_section_items_cv_custom_sections` (`sectionId`),
  CONSTRAINT `FK_cv_custom_section_items_cv_custom_sections` FOREIGN KEY (`sectionId`) REFERENCES `cv_custom_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_custom_section_items`
--

LOCK TABLES `cv_custom_section_items` WRITE;
/*!40000 ALTER TABLE `cv_custom_section_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `cv_custom_section_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_custom_sections`
--

DROP TABLE IF EXISTS `cv_custom_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_custom_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`,`cvId`),
  KEY `FK_cv_custom_groups_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_custom_groups_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_custom_sections`
--

LOCK TABLES `cv_custom_sections` WRITE;
/*!40000 ALTER TABLE `cv_custom_sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `cv_custom_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_educations`
--

DROP TABLE IF EXISTS `cv_educations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `dateFrom` date DEFAULT NULL,
  `dateTo` date DEFAULT NULL,
  `title` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `level` varchar(80) CHARACTER SET latin1 DEFAULT NULL,
  `school` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`,`cvId`),
  KEY `FK_cv_educations_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_educations_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_educations`
--

LOCK TABLES `cv_educations` WRITE;
/*!40000 ALTER TABLE `cv_educations` DISABLE KEYS */;
INSERT INTO `cv_educations` VALUES (137,51,'2012-12-19',NULL,'test 1234','isced3','sdfds',3),(141,51,'2012-11-05','2012-11-15','sdfsdfsdfsd','isced4','sdfds',1),(143,51,'2012-11-13','2012-11-17','gfdgdf','isced2','dfdsf',2);
/*!40000 ALTER TABLE `cv_educations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_language_skills`
--

DROP TABLE IF EXISTS `cv_language_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_language_skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `language` varchar(255) DEFAULT NULL,
  `rating` set('A1','A2','B1','B2','C1','C2') DEFAULT NULL,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `language` (`language`,`cvId`),
  KEY `FK_cv_language_skills_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_language_skills_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_language_skills`
--

LOCK TABLES `cv_language_skills` WRITE;
/*!40000 ALTER TABLE `cv_language_skills` DISABLE KEYS */;
INSERT INTO `cv_language_skills` VALUES (137,51,'english','B1',NULL),(138,51,'deutsch','C1',NULL),(143,51,'albanish','C2',NULL);
/*!40000 ALTER TABLE `cv_language_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_personaldata`
--

DROP TABLE IF EXISTS `cv_personaldata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_personaldata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL DEFAULT '0',
  `gender` set('NotKnown','Male','Female','NotSpecified') DEFAULT 'NotKnown',
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `martialStatus` set('DomesticPartner','Divorced','Married','Unreported','Separated','Unmarried','Widowed','LegallySeparated') DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `addContactData` text,
  `nationality` varchar(255) DEFAULT NULL,
  `secondNationality` varchar(255) DEFAULT NULL,
  `hobbies` text,
  `driverLicense` varchar(255) DEFAULT NULL,
  `noticePeriod` varchar(255) DEFAULT NULL,
  `createDate` bigint(20) unsigned DEFAULT NULL,
  `updateDate` bigint(20) unsigned DEFAULT NULL,
  `passphoto_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cvId` (`cvId`),
  KEY `FK_cv_personaldata_cv_attachments` (`passphoto_id`),
  CONSTRAINT `FK_cv_personaldata_cv_attachments` FOREIGN KEY (`passphoto_id`) REFERENCES `cv_attachments` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_cv_personaldata_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_personaldata`
--

LOCK TABLES `cv_personaldata` WRITE;
/*!40000 ALTER TABLE `cv_personaldata` DISABLE KEYS */;
INSERT INTO `cv_personaldata` VALUES (1,51,'Male','bukimuki','muqa','1988-08-10',NULL,NULL,'Chropfacher 6','8603','Schwerzenbach','Switzerland','49448251108','49448251108',NULL,NULL,NULL,NULL,NULL,NULL,'C,A,D1',NULL,1356785921,1356785921,344),(2,52,NULL,'hans','muster',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1358196469,1358196469,NULL);
/*!40000 ALTER TABLE `cv_personaldata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_references`
--

DROP TABLE IF EXISTS `cv_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_references` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `contactData` text,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`cvId`),
  KEY `FK_cv_references_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_references_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_references`
--

LOCK TABLES `cv_references` WRITE;
/*!40000 ALTER TABLE `cv_references` DISABLE KEYS */;
INSERT INTO `cv_references` VALUES (135,51,'hans muster','abb','test',NULL);
/*!40000 ALTER TABLE `cv_references` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_shares`
--

DROP TABLE IF EXISTS `cv_shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_shares` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId` (`userId`,`cvId`,`email`),
  KEY `FK_cv_shares_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_shares_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_resume_shares_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_shares`
--

LOCK TABLES `cv_shares` WRITE;
/*!40000 ALTER TABLE `cv_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `cv_shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_work_experiences`
--

DROP TABLE IF EXISTS `cv_work_experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_work_experiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `dateFrom` date DEFAULT NULL,
  `dateTo` date DEFAULT NULL,
  `stillWorking` tinyint(3) unsigned DEFAULT NULL,
  `profession` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `employment` set('temporary','freelance','practica','sideline','permanent','apprenticeship') DEFAULT NULL,
  `company` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `activities` text,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cv_work_experiences_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_work_experiences_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=429 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_work_experiences`
--

LOCK TABLES `cv_work_experiences` WRITE;
/*!40000 ALTER TABLE `cv_work_experiences` DISABLE KEYS */;
INSERT INTO `cv_work_experiences` VALUES (428,51,'2012-11-13','2012-11-15',1,'maler','temporary','test','test',NULL);
/*!40000 ALTER TABLE `cv_work_experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_work_skills`
--

DROP TABLE IF EXISTS `cv_work_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_work_skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cvId` int(10) unsigned NOT NULL,
  `skill` varchar(255) DEFAULT NULL,
  `rating` int(10) unsigned DEFAULT NULL,
  `sortPosition` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `skill` (`skill`,`cvId`),
  KEY `FK_cv_work_skills_user_cvs` (`cvId`),
  CONSTRAINT `FK_cv_work_skills_user_cvs` FOREIGN KEY (`cvId`) REFERENCES `user_cvs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_work_skills`
--

LOCK TABLES `cv_work_skills` WRITE;
/*!40000 ALTER TABLE `cv_work_skills` DISABLE KEYS */;
INSERT INTO `cv_work_skills` VALUES (174,51,'ddddddddd',3,NULL),(176,51,'vvvvvvvv',3,NULL),(178,51,'bbbbbbbb',5,NULL);
/*!40000 ALTER TABLE `cv_work_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_cvs`
--

DROP TABLE IF EXISTS `user_cvs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_cvs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `updateDate` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `format` int(10) unsigned DEFAULT NULL,
  `layout` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`,`userId`),
  KEY `FK_user_resumes_users` (`userId`),
  CONSTRAINT `FK_user_resumes_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_cvs`
--

LOCK TABLES `user_cvs` WRITE;
/*!40000 ALTER TABLE `user_cvs` DISABLE KEYS */;
INSERT INTO `user_cvs` VALUES (51,259,NULL,NULL,NULL,NULL,NULL),(52,85,1358196469,1358196469,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_cvs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gender` set('NotKnown','Male','Female','NotSpecified') NOT NULL DEFAULT 'NotKnown',
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createDate` bigint(20) unsigned DEFAULT NULL,
  `updateDate` bigint(20) unsigned DEFAULT NULL,
  `isActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `confirmCode` varchar(255) DEFAULT NULL,
  `loginAttempt` int(1) DEFAULT '0',
  `resetPasswordCode` varchar(255) DEFAULT NULL,
  `salt` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (64,'NotKnown','hans','muster','1995-04-05','h.muster@hotmail.com',NULL,'123456',100200300,400500600,1,'6ce1dd9294a530a0020164e59716dd8d',0,NULL,NULL),(80,'NotKnown','hans','muster','1995-04-05','user.seed@cvcreator.ch',NULL,'123456',100200300,400500600,1,'ebef7741b0702fc26b3603fac891c12b',0,NULL,NULL),(85,'Male','hans','muster','0000-00-00','burim_m@hotmail.com','burim','f7c3bc1d808e04732adf679965ccc34ca7ae3441',100200300,NULL,1,NULL,NULL,'477946f3ce38d1f675cc9d9dae11fae7686535b7',NULL),(259,'Male','bukimuki','muqa','1979-10-12','burim@muqa.ch','bmuqa','8ca4ad6282b8b69f3cb5587995fd9dd787a6b9b1',NULL,NULL,1,NULL,NULL,NULL,'e708592c5b080273d1c7f62536caa19e'),(260,'Male','burim','muqa',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(261,'Male','buki','muqa',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(262,'Male','bukimuki','muqa',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(263,'Male','bukimuki','muqa',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(264,'Male','bukimuki','muqa',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(265,'Male','bukimuki','muqa',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(266,'Male','test1','test1','1995-02-01','test1@muqa.ch','test1','f53e41cf5ef1e5b0cac8b9e54b82c34f05a5c43a',NULL,NULL,1,NULL,NULL,NULL,'52f0fde5bf230c3560aeaf2f1b9a934d'),(267,'Male','test2','test2','2003-02-12','test2@muqa.ch','test2','d0a48d7f8a7759ac253d8e9c4d875c895d3d57d9',NULL,NULL,1,NULL,NULL,NULL,'b462adf6addd1827a1fe2a80f7f03587'),(268,'Male','test3','test3','2003-02-12','test3@muqa.ch','test3','4dac63addc155bcde92da160fbbe8dec9398d35c',NULL,NULL,1,NULL,NULL,NULL,'09ee5cc3e1037d05df65650cea6b7e23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cvcreator'
--

--
-- Dumping routines for database 'cvcreator'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-24 18:07:03
