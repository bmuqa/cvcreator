(function($) {

    $.fn.updateCvSection = function(redirectUrl) {
        if (undefined != $(this).data('update-url') && '' != $(this).data('update-url')) {
            $.ajax({
                url: $(this).data('update-url'),
                context: this,
                success: function (data) {
                    $(this).html(data);
                },
                complete: function (data) {
                    redirectUrl = redirectUrl || false;
                    if (redirectUrl != false) {
                        setTimeout(function() {
                            console.log(redirectUrl);
                            location.href = redirectUrl;
                        }, 1000);
                    } else {
                        window.parent.$('.modal').modal('hide');
                    }
                    $('#ajaxloader-container').hide();
                },
                error: function(qXHR, textStatus, errorThrown) {
                    console.log(qXHR);
                    $('#ajaxloader-container').hide();
                }
            });
        }
    };

    $.fn.deleteCvPart = function(formName) {
        if (undefined != $(this).data('url')) {
            $('#ajaxloader-container').show();
            $.ajax({ url: $(this).data('url'),
                success: function (data) {
                    $('#section-' + formName).updateCvSection();
                },
                complete: function () {
                    $('#ajaxloader-container').hide();
                }
            });
        }
    }

    $.fn.deleteCvSection = function(formName) {
        if (undefined != $(this).data('url')) {
            $('#ajaxloader-container').show();
            $.ajax({ url: $(this).data('url'),
                success: function (data) {
                    $('#section-head-' + formName).remove();
                    $('#section-' + formName).remove();
                },
                complete: function () {
                    $('#ajaxloader-container').hide();
                }
            });
        }
    }

    $.fn.moveCvSection = function() {
        if (undefined != $(this).data('url')) {
            $('#ajaxloader-container').show();
            $.ajax({ url: $(this).data('url'),
                success: function (data) {
                    location.reload();
                },
                complete: function () {
                    $('#ajaxloader-container').hide();
                }
            });
        }
    }

    $.fn.moveCvPart = function(formName) {
        if (undefined != $(this).data('url')) {
            $('#ajaxloader-container').show();
            $.ajax({ url: $(this).data('url'),
                success: function (data) {
                    $('#section-' + formName).updateCvSection();
                },
                complete: function () {
                    $('#ajaxloader-container').hide();
                }
            });
        }
    }


})(jQuery);