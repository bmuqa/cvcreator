(function ($) {

    $modal =  $('<div class="modal hide fade"><div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
        '<h3 id="modal-title"></h3></div><div class="modal-body" ></div></div>');

    $.fn.openModal = function() {
        var url = $(this).attr('url');
        var modalTitle = $(this).data('modal-title');
        var width =  $(this).data('width') ?  $(this).data('width') : '660';
        var height =  $(this).data('height') ?  $(this).data('height') : '390';

        $modal.find('#modal-title').html(modalTitle);
        $modal.find('.modal-body').addClass('modal-loading');
        $modal.find('.modal-body').html(
            '<iframe width="100%" height="' + (height - 50) + '" frameborder="0" scrolling="yes" allowtransparency="true" src="' + url + '"></iframe>');

        $modal.modal({ show: true }).css({
            width: function () { return width + 'px'; },
            height: function () { return height + 'px'; },
            'margin-left': function () { return -($(this).width() / 2); },
            'margin-top': function () { return -($(this).height() / 2); }
        });

        $modal.find('.modal-body').css({
            height: function() { return height + 'px'; },
            maxHeight: function() { return (height - 50) + 'px'; }
        });

        $modal.on('hidden', function () {
            $(this).find('.modal-body').html('');
            $(this).remove();
        });
    };

}(jQuery));

