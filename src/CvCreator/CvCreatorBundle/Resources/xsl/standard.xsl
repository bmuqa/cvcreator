<?xml version="1.0" encoding="UTF-8"?>
<!--

-->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:html="http://www.w3.org/1999/xhtml">

    <xsl:import href="lib/W3C-REC.xsl"/>

    <xsl:output method="xml"
                version="1.0"
                encoding="UTF-8"
                indent="no"/>

    <!--======================================================================
        Parameters
    =======================================================================-->

    <!-- page size -->
    <xsl:param name="page-width">auto</xsl:param>
    <xsl:param name="page-height">auto</xsl:param>
    <xsl:param name="page-margin-top">2cm</xsl:param>
    <xsl:param name="page-margin-bottom">1cm</xsl:param>
    <xsl:param name="page-margin-left">2cm</xsl:param>
    <xsl:param name="page-margin-right">2cm</xsl:param>

    <!-- page header and footer -->
    <xsl:param name="page-header-margin">0.5in</xsl:param>
    <xsl:param name="page-footer-margin">0.5in</xsl:param>
    <xsl:param name="title-print-in-header">true</xsl:param>
    <xsl:param name="page-number-print-in-footer">true</xsl:param>

    <!-- multi column -->
    <xsl:param name="column-count">1</xsl:param>
    <xsl:param name="column-gap">18pt</xsl:param>

    <!-- writing-mode: lr-tb | rl-tb | tb-rl -->
    <xsl:param name="writing-mode">lr-tb</xsl:param>

    <!-- text-align: justify | start -->
    <xsl:param name="text-align">justify</xsl:param>

    <!-- hyphenate: true | false -->
    <xsl:param name="hyphenate">true</xsl:param>


    <!--======================================================================
        Attribute Sets
    =======================================================================-->


    <!--======================================================================
       Block-level
    =======================================================================-->

    <!--======================================================================
      define custom styles
    =======================================================================-->

    <xsl:attribute-set name="cv-head-style">
        <xsl:attribute name="position">relative</xsl:attribute>
        <xsl:attribute name="width">16cm</xsl:attribute>
        <xsl:attribute name="font-size">11pt</xsl:attribute>
        <xsl:attribute name="font-family">Helvetica</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="section-style">
        <xsl:attribute name="space-before">1cm</xsl:attribute>
        <xsl:attribute name="space-after">1cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="section-head-style">
        <xsl:attribute name="position">relative</xsl:attribute>
        <xsl:attribute name="width">16cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="section-head-title-style">
        <xsl:attribute name="space-before">0em</xsl:attribute>
        <xsl:attribute name="space-after">1em</xsl:attribute>
        <xsl:attribute name="font-size">12pt</xsl:attribute>
        <xsl:attribute name="font-family">Helvetica</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-style">
        <xsl:attribute name="font-size">11pt</xsl:attribute>
        <xsl:attribute name="font-family">Helvetica</xsl:attribute>
        <xsl:attribute name="width">16cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="row-style">
        <xsl:attribute name="space-after">1cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell-style">
    </xsl:attribute-set>

    <!--======================================================================
      match styles with classes
    =======================================================================-->

    <xsl:template match="html:div[@class = 'cv-head']">
        <fo:block-container xsl:use-attribute-sets="cv-head-style">
            <fo:block start-indent="0pt" end-indent="0pt"><xsl:apply-templates/></fo:block>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="html:div[@class = 'section']">
        <fo:block-container xsl:use-attribute-sets="section-style">
            <fo:block start-indent="0pt" end-indent="0pt"><xsl:apply-templates/></fo:block>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="html:div[@class = 'section-head']">
        <fo:block-container xsl:use-attribute-sets="section-head-style" keep-with-next.within-page="always">
            <fo:block start-indent="0pt" end-indent="0pt"><xsl:apply-templates/></fo:block>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="html:p[@class = 'section-head-title']">
        <fo:block xsl:use-attribute-sets="section-head-title-style">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="html:table[@class = 'table-2col']">
        <fo:block start-indent="0pt" xsl:use-attribute-sets="table-style">
            <fo:table table-layout="fixed" width="16cm">
                <fo:table-column column-width="4cm"/>
                <fo:table-column column-width="12cm"/>
                <fo:table-body>
                    <xsl:apply-templates/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="html:table[@class = 'table-3col']">
        <fo:block xsl:use-attribute-sets="table-style">
            <fo:table table-layout="fixed" width="16cm">
                <fo:table-column column-width="4cm"/>
                <fo:table-column column-width="6cm"/>
                <fo:table-column column-width="6cm"/>
                <fo:table-body>
                    <xsl:apply-templates/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="html:tr">
        <fo:table-row xsl:use-attribute-sets="row-style" keep-together.within-page="always"><xsl:apply-templates/></fo:table-row>
    </xsl:template>

    <xsl:template match="html:td">
        <fo:table-cell xsl:use-attribute-sets="cell-style" padding-before="3pt" padding-after="3pt" display-align="before">
            <fo:block><xsl:apply-templates/></fo:block>
        </fo:table-cell>
    </xsl:template>

    <xsl:template match="html:div[@class = 'one-image-per-page']">
        <fo:block-container page-break-before="always">
            <fo:block start-indent="0pt" end-indent="0pt"><xsl:apply-templates/></fo:block>
        </fo:block-container>
    </xsl:template>

</xsl:stylesheet>
