<?php

namespace CvCreator\CvCreatorBundle\Tests\Controller;

use CvCreator\CvCreatorBundle\Controller;
use CvCreator\CvCreatorBundle\Entity;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CvControllerTest extends WebTestCase
{
    private $testCvId;
    private $testUserName;
    private $testUserPassword;
    private $testUserId;
    private $cleanUpUserIds = array();
    private $cleanUpAttachmentIds = array();
    private $tmpTestImg;
    private $testImg;

    public function setUp()
    {
        $this->testUserName = 'bmuqa';
        $this->testUserPassword = '123456';
        $this->testUserId = 0;
        $this->testCvId = 51;

        $this->tmpTestImg = dirname(__FILE__) . '/../Data/tmp/test.jpg';
        $this->testImg = dirname(__FILE__) . '/../Data/test.jpg';

        parent::setUp();
    }

    public function tearDown()
    {
        $client = $this->getTestClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        // delete all temporary users
        foreach($this->cleanUpUserIds as $user_id) {
            $user = $entityManager->getRepository('CvCreatorBundle:User')->find($user_id);
            $entityManager->remove($user);
        }

        // delete all temporary users
        foreach($this->cleanUpAttachmentIds as $attachment_id) {
            $attachment = $entityManager->getRepository('CvCreatorBundle:CvAttachment')->find($attachment_id);
            $entityManager->remove($attachment);
        }

        $entityManager->flush();
        $this->cleanUpUserIds = array();
        $this->cleanUpAttachmentIds = array();

        parent::tearDown();
    }

    private function getTestClient($username = null, $password = null)
    {
        return static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => $username ? $username : $this->testUserName,
                'PHP_AUTH_PW'   => $password ? $password : $this->testUserPassword,
            )
        );
    }

    private function getNewTestUser()
    {
        $client = $this->getTestClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        $user = new \CvCreator\CvCreatorBundle\Entity\User();
        $user->setUsername('testuser' . time());
        $user->setPassword('testuser');
        $user->setSalt('test');
        $user->setGender('male');
        $user->setIsActive(true);

        $encoderFactory = $client->getContainer()->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);

        $entityManager->persist($user);
        $entityManager->flush();

        $this->cleanUpUserIds[] = $user->getId();

        $cv = new Entity\Cv();
        $cv->setUser($user)
           ->setCreateDate(time())
           ->setUpdateDate(time());

        $entityManager->persist($cv);
        $entityManager->flush();

        return $user;
    }

    public function testIndexAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/');

        $this->assertTrue($crawler->filter('html:contains("Einfach, praktisch und kostenlos")')->count() > 0);
    }

    public function testCreateAction()
    {
        $newUser = $this->getNewTestUser();

        $client = $this->getTestClient($newUser->getUsername(), 'testuser');

        // check error handling if cv id is not available
        $crawler = $client->request('GET', '/cv/create/' . time());
        $this->assertTrue($crawler->filter('div.cv-section-body')->count() == 0);

        $crawler = $client->request('GET', '/cv/create/0');
        $this->assertTrue($crawler->filter('div.cv-section-body')->count() > 7);

        $client = $this->getTestClient();

        $crawler = $client->request('GET', '/cv/create/0');
        $this->assertTrue($crawler->filter('div.cv-section-body')->count() == 7);

        $crawler = $client->request('GET', '/cv/create/' . $this->testCvId);
        $this->assertTrue($crawler->filter('div.cv-section-body')->count() == 7);
    }

    public function testEditAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/edit/' . $this->testCvId . '/personaldata/1');
        $this->assertTrue($crawler->filter('html:contains("Anrede")')->count() > 0);
    }

    public static function saveActionProvider()
    {
        if (!file_exists(dirname(__FILE__) . '/../Data/tmp/passphoto.jpg')) {
            // prepare temporary file for passphoto and attachment fields
            $testImgContent = file_get_contents(dirname(__FILE__) . '/../Data/passphoto.jpg');
            file_put_contents(dirname(__FILE__) . '/../Data/tmp/passphoto.jpg', $testImgContent);
        }

        $passphoto = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            dirname(__FILE__) . '/../Data/tmp/passphoto.jpg',
            'test.jpg'
        );

        if (!file_exists(dirname(__FILE__) . '/../Data/tmp/test.jpg')) {
            // prepare temporary file for passphoto and attachment fields
            $testImgContent = file_get_contents(dirname(__FILE__) . '/../Data/test.jpg');
            file_put_contents(dirname(__FILE__) . '/../Data/tmp/test.jpg', $testImgContent);
        }

        $attachment = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            dirname(__FILE__) . '/../Data/tmp/test.jpg',
            'test.jpg'
        );

        return array(
            array('type' => 'attachment', 'entity' => 'CvAttachment', 'data' => array(
                'attachment[type]' => 'workCertificate',
                'attachment[file]' => $attachment,
            )),
            array('type' => 'education', 'entity' => 'CvEducation', 'data' => array(
                'education[title]' => 'test education',
                'education[school]' => 'test school',
                'education[level]' => 'isced4',
                'education[dateFrom]' => '01.04.2013',
                'education[dateTo]' => '05.06.2013',
            )),
            array('type' => 'languageskill', 'entity' => 'CvLanguageSkill', 'data' => array(
                'languageskill[language]' => 'deutsch',
                'languageskill[rating]' => 'A2'
            )),
            array('type' => 'personaldata', 'entity' => 'CvPersonalData', 'data' => array(
                'personaldata[passphoto]' => $passphoto,
                'personaldata[martialStatus]' => 'Married',
                'personaldata[address]' => 'test streeet',
                'personaldata[zip]' => '8008',
                'personaldata[city]' => 'Zürich',
                'personaldata[country]' => 'Schweiz',
                'personaldata[phone]' => '+4123456789',
                'personaldata[mobile]' => '+4123456789',
                'personaldata[fax]' => '+4123456789',
                'personaldata[email]' => 'max@muster.com',
                'personaldata[web]' => 'http://www.google.com',
                'personaldata[addContactData]' => 'IM: max@muster.com, facebook: http://www.facebook.com/max',
                'personaldata[nationality]' => 'Schweizer',
                'personaldata[secondNationality]' => 'Kosovo',
                'personaldata[driverLicense]' => array('A'),
                'personaldata[noticePeriod]' => '3 Monate',
                'personaldata[hobbies]' => 'Tennis, Fussball, Piano',
            )),
            array('type' => 'reference', 'entity' => 'CvReference', 'data' => array(
                'reference[name]' => 'max muster',
                'reference[company]' => 'max company',
                'reference[contactData]' => 'email: max@muster.com, fax: +412345678',
            )),
            array('type' => 'workexperience', 'entity' => 'CvWorkExperience', 'data' => array(
                'workexperience[profession]' => 'test profession',
                'workexperience[employment]' => 'freelance',
                'workexperience[company]' => 'test company',
                'workexperience[stillWorking]' => false,
                'workexperience[dateFrom]' => '22.05.2010',
                'workexperience[dateTo]' => '22.07.2012',
                'workexperience[activities]' => 'test, others, nothing',
            )),
            array('type' => 'workskill', 'entity' => 'CvWorkSkill', 'data' => array(
                'workskill[skill]' => 'test skill',
                'workskill[rating]'  => 4
            )),
        );
    }

    /**
     * @dataProvider saveActionProvider
     */
    public function testSaveAction($type, $entity, $data)
    {
        $newUser = $this->getNewTestUser();
        $client = $this->getTestClient($newUser->getUsername(), 'testuser');
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        /** @var $cv Entity\Cv */
        $cv = $entityManager->getRepository('CvCreatorBundle:Cv')->findOneByUser($newUser);

        $itemId = 0;
        if ($type == 'personaldata') {
            $personaldata = $entityManager->getRepository('CvCreatorBundle:CvPersonalData')->findOneByCv($cv);
            if (!empty($personaldata)) {
                $itemId = $personaldata->getId();
            }
        }

        // open the form to save a new item
        $crawler = $client->request('GET', '/cv/save/' . $cv->getId() . '/' . $type . '/' . $itemId);
        $this->assertTrue($crawler->filter('form.form')->count() > 0);

        // send form to save a new item
        $submitButton = $crawler->selectButton('Speichern');
        $form = $submitButton->form($data);

        $crawler = $client->submit($form);

        // check if success message was showed
        $this->assertTrue($crawler->filter('.alert-success')->count() > 0);

        $savedItem = $entityManager->getRepository('CvCreatorBundle:' . $entity)->findOneByCv($cv);

        // check if item was saved to database
        $this->assertNotNull($savedItem);
    }

    public function testAddPartAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/add/' . $this->testCvId . '/education/0');
        $this->assertTrue($crawler->filter('html:contains("Schule")')->count() > 0);
    }

    public function testDeleteSectionAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/delete-section/section/1234');
        $this->assertTrue($crawler->filter('html:contains("deleted")')->count() > 0);
    }


    public function testMoveSectionAction()
    {
        $client = $this->getTestClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        /** @var $cv Entity\Cv */
        $cv = $entityManager->getRepository('CvCreatorBundle:Cv')->find($this->testCvId);
        $section = $entityManager->getRepository('CvCreatorBundle:CvSection')->findOneBy(
            array('cv' => $cv, 'sortposition' => '2'));

        $sectionId = $section->getId();
        $sectionSortPosition = $section->getSortPosition();

        $crawler = $client->request(
            'GET',
            '/cv/move_section/' . $this->testCvId . '/' . $sectionId . '/up'
        );

        $sectionMoved = $entityManager->getRepository('CvCreatorBundle:CvSection')->find($sectionId);
        $this->assertEquals($sectionMoved->getSortPosition(), ($sectionSortPosition - 1));
    }

    public function testMoveSectionPartAction()
    {
        $client = $this->getTestClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        /** @var $cv Entity\Cv */
        $cv = $entityManager->getRepository('CvCreatorBundle:Cv')->find($this->testCvId);
        $educations = $entityManager->getRepository('CvCreatorBundle:CvEducation')->findByCv($cv);

        $educationIdToMove = $educationSortPosition = 0;
        foreach($educations as $education) {
            if ($educationIdToMove == 0 && $education->getSortPosition() > 1) {
                $educationSortPosition = $education->getSortposition();
                $educationIdToMove = $education->getId();
            }
        }

        $crawler = $client->request(
            'GET',
            '/cv/move-part-up/' . $this->testCvId . '/education/' . $educationIdToMove
        );

        $educationMoved = $entityManager->getRepository('CvCreatorBundle:CvEducation')->find($educationIdToMove);
        $this->assertEquals($educationSortPosition, ($educationMoved->getSortPosition() + 1));
    }

    public function testDeletePartAction()
    {
        $client = $this->getTestClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        $cv = $entityManager->getRepository('CvCreatorBundle:Cv')->find($this->testCvId);
        $workSkill = new Entity\CvWorkSkill();
        $workSkill->setCv($cv);
        $workSkill->setSortposition(0);
        $workSkill->setSkill('test');
        $workSkill->setRating(3);

        $entityManager->persist($workSkill);
        $entityManager->flush();

        $workSkillToDeleteId = $workSkill->getId();

        $this->assertNotEmpty($entityManager->getRepository('CvCreatorBundle:CvWorkSkill')->find($workSkillToDeleteId));

        $crawler = $client->request('GET', '/cv/delete-part/' . $this->testCvId . '/workskill/' . $workSkillToDeleteId);

        $this->assertEmpty($entityManager->getRepository('CvCreatorBundle:CvWorkSkill')->find($workSkillToDeleteId));
    }

    /**
     * @runInSeparateProcess
     */
    public function testGeneratePdfAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/generate/' . $this->testCvId . '/standard/pdf/0');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotEmpty($client->getResponse()->getContent());
    }

    /**
     * @runInSeparateProcess
     */
    public function testGenerateRtfAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/generate/' . $this->testCvId . '/standard/rtf/0');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotEmpty($client->getResponse()->getContent());
    }

    public function testGetSectionDataAction()
    {
        $client = $this->getTestClient();
        $crawler = $client->request('GET', '/cv/get-section/' . $this->testCvId . '/education');
        $this->assertTrue($crawler->filter('#section-education')->count() > 0);
    }

    /**
     * @expectException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testGetAttachmentAction()
    {
        $client = $this->getTestClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        /** @var $cv Entity\Cv */
        $cv = $entityManager->getRepository('CvCreatorBundle:Cv')->find($this->testCvId);

        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $this->assertNull($attachment->upload());

        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;
        $attachment->setCv($cv);
        //$attachment->upload();

        $entityManager->persist($attachment);
        $entityManager->flush();

        $this->cleanUpAttachmentIds[] = $attachment->getId();

        $client->request('GET', '/cv/attachment/xxxx/' . $attachment->getId() . '/1');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        $client->request('GET', '/cv/attachment/' . $cv->getId() . '/xxxx/1');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        $client->request('GET', '/cv/attachment/' . $cv->getId() . '/' . $attachment->getId() . '/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertNotEmpty($client->getResponse()->getContent());
    }
}
