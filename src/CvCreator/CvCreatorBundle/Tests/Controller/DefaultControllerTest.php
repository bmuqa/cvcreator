<?php

namespace CvCreator\CvCreatorBundle\Tests\Controller;

use CvCreator\CvCreatorBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertTrue($crawler->filter('html:contains("Willkommen")')->count() > 0);
    }

    public function testDisclaimerAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/disclaimer');
        $this->assertTrue($crawler->filter('html:contains("Über uns")')->count() > 0);
    }

    public function testHelpAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/help');
        $this->assertTrue($crawler->filter('html:contains("Info")')->count() > 0);
    }

    public function testContactAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/contact');
        $this->assertTrue($crawler->filter('html:contains("Kontakt")')->count() > 0);

        // send contact form
        $submitButton = $crawler->selectButton('Senden');
        $form = $submitButton->form(array(
            'contact[name]'   => 'test',
            'contact[email]'  => 'test@test.com',
            'contact[message]'  => 'Das ist eine E-Mail aus den Unit tests.',
        ));

        $crawler = $client->submit($form);

        // false because of captcha validation
        $this->assertFalse($crawler->filter('.alert-success')->count() > 0);
    }
}
