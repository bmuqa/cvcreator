<?php

namespace CvCreator\CvCreatorBundle\Tests\Controller;

use CvCreator\CvCreatorBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    protected $userId;
    protected $cleanUpUsers;

    public function setUp()
    {
        $this->userId = 259;
        $this->cleanUpUsers = array();
        parent::setUp();
    }

    public function tearDown()
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        // delete all temporary users
        foreach($this->cleanUpUsers as $userId) {
            $user = $entityManager->getRepository('CvCreatorBundle:User')->find($userId);
            $entityManager->remove($user);
        }

        $entityManager->flush();

        $this->cleanUpUsers = array();

        parent::tearDown();
    }

    public function testProfileAction()
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'bmuqa',
                'PHP_AUTH_PW'   => '123456',
            )
        );
        $crawler = $client->request('GET', '/user/profile');
        $this->assertTrue($crawler->filter('html:contains("Profil-Einstellungen")')->count() > 0);
    }

    public function testChangePasswordAction()
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'bmuqa',
                'PHP_AUTH_PW'   => '123456',
            )
        );

        $crawler = $client->request('GET', '/user/change/password');
        $this->assertTrue($crawler->filter('#changePasswordSaveButton')->count() > 0);

        // send form
        $submitButton = $crawler->selectButton('changePasswordSaveButton');
        $form = $submitButton->form(array(
            'changepassword[password][first]' => '123456',
            'changepassword[password][second]' => '123456',
            'changepassword[password_old]' => '123456'
        ));

        $crawler = $client->submit($form);

        // check if the data was successful saved
        $this->assertTrue($crawler->filter('.alert-success')->count() > 0);
    }

    public function testDeleteAction()
    {
        $userName = 'test' . time();
        $password_raw = '1234';

        $client = static::createClient();

        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        $user = new \CvCreator\CvCreatorBundle\Entity\User();
        $user->setUsername($userName);
        $user->setSalt('test');
        $user->setGender('male');
        $user->setIsActive(true);

        $encoderFactory = $client->getContainer()->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($password_raw, $user->getSalt());
        $user->setPassword($password);

        $entityManager->persist($user);
        $entityManager->flush();

        $userId = $user->getId();

        // create a storage user dir
        $storagePath = realpath($client->getContainer()->getParameter('resources.storage.path'));
        if (!is_dir($storagePath . DIRECTORY_SEPARATOR . $userId)) {
            mkdir($storagePath . DIRECTORY_SEPARATOR . $userId);
            mkdir($storagePath . DIRECTORY_SEPARATOR . $userId . DIRECTORY_SEPARATOR . 'test');
            mkdir($storagePath . DIRECTORY_SEPARATOR . $userId . DIRECTORY_SEPARATOR . 'test' . DIRECTORY_SEPARATOR . 'pdf');
        }

        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => $user->getUsername(),
                'PHP_AUTH_PW'   => $password_raw,
            )
        );


        // check the output
        $crawler = $client->request('GET', '/user/delete');

        $this->assertTrue($crawler->filter('html:contains("Ihr Konto wurde erfolgreich gelöscht.")')->count() > 0);

        // check if user files was deleted
        $this->assertFalse(is_dir($storagePath . DIRECTORY_SEPARATOR . $userId));

        // check if user is logged out
        $this->assertNull($client->getContainer()->get('security.context')->getToken());

        // check if user was deleted
        $this->assertNull(
            $client->getContainer()
                   ->get('doctrine.orm.entity_manager')
                   ->getRepository('CvCreatorBundle:User')
                   ->find($userId)
        );

        //$this->cleanUpUsers[] = $userId;
    }



    public function testLoginAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/user/login');
        $this->assertTrue($crawler->filter('html:contains("Login")')->count() > 0);

        // send form with wrong login data
        $submitButton = $crawler->selectButton('Login');
        $form = $submitButton->form(array(
            '_username' => 'bmuqa',
            '_password' => 'xxxxx'
        ));

        $crawler = $client->submit($form);

        $this->assertFalse($client->getResponse()->isRedirect());

        // send form with right login data
        $submitButton = $crawler->selectButton('Login');
        $form = $submitButton->form(array(
            '_username' => 'bmuqa',
            '_password' => '123456'
        ));

        $crawler = $client->submit($form);

        // Assert that the response is a redirect
        $this->assertTrue($client->getResponse()->isRedirect());
    }


    public function logoutAction()
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'bmuqa',
                'PHP_AUTH_PW'   => '123456',
            )
        );

        $crawler = $client->request('GET', '/user/logout');
        $this->assertTrue($crawler->filter('html:contains("Willkommen")')->count() > 0);
    }

    public function testSignAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/user/sign');
        $this->assertTrue($crawler->filter('html:contains("Registrieren")')->count() > 0);


//
//        // send form
//        $submitButton = $crawler->selectButton('Login');
//        $form = $submitButton->form(array(
//            '_username' => '123456',
//            '_password' => '123456'
//        ));
//
//        $crawler = $client->submit($form);
//
//        // check if the profile navigation point is visible
//        $this->assertTrue($crawler->filter('a.profile')->count() > 0);


    }

    public function testSignConfirmAction()
    {
        $userId = time();
        $userName = 'test' . time();
        $password = '1234';

        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine')->getEntityManager();

        $user = new \CvCreator\CvCreatorBundle\Entity\User();
        $user->setId($userId);
        $user->setUsername($userName);
        $user->setPassword($password);
        $user->setSalt('test');
        $user->setGender('male');
        $user->setIsActive(true);
        $user->setConfirmcode('test');

        $encoderFactory = $client->getContainer()->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);

        $entityManager->persist($user);
        $entityManager->flush();

        $crawler = $client->request('GET', '/user/sign/confirm/' . $user->getId() . '/' . $user->getConfirmcode());

        $this->assertTrue($crawler->filter('.alert-success')->count() > 0);
        $this->cleanUpUsers[] = $user->getId();
    }

    public function testResetPasswordAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/user/reset/password');

        $this->assertTrue($crawler->filter('#resetpassword_email')->count() > 0);
    }

    public function testNewPasswordAction()
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        $resetCode = md5(time());

        /** @var $user \CvCreator\CvCreatorBundle\Entity\User */
        $user = $entityManager->getRepository('CvCreatorBundle:User')->find($this->userId);
        $user->setResetpasswordcode($resetCode);
        $entityManager->flush();

        $crawler = $client->request('GET', '/user/new/password/' . $this->userId . '/xxxxx');

        // check error because the reset code is not available

        $this->assertFalse($crawler->filter('#newpassword_password_first')->count() > 0);

        $crawler = $client->request('GET', '/user/new/password/' . $user->getId() . '/' . $resetCode);

        // check if form is visible
        $this->assertTrue($crawler->filter('#newpassword_password_first')->count() > 0);

        // send form
        $submitButton = $crawler->selectButton('Speichern');
        $form = $submitButton->form(array(
            'newpassword[password][first]' => '123456',
            'newpassword[password][second]' => '123456'
        ));

        $crawler = $client->submit($form);

        // check if the profile navigation point is visible
        $this->assertTrue($crawler->filter('.alert-success')->count() > 0);
    }
}
