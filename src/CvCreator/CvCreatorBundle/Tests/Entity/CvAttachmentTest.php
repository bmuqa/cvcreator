<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

require_once dirname(__DIR__).'/../../../../app/AppKernel.php';

class CvAttachmentTest extends \PHPUnit_Framework_TestCase
{
    protected $tmpFile;

    /**
     * @var \Symfony\Component\HttpKernel\AppKernel
     */
    protected $kernel;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    protected $cvId = 51;
    protected $tmpTestImg;
    protected $testImg;


    public function setUp()
    {
        // Boot the AppKernel in the test environment and with the debug.
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        // Store the container and the entity manager in test case properties
        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();

        $this->tmpTestImg = dirname(__FILE__) . '/../Data/tmp/test.jpg';
        $this->tmpTestImgThumb = dirname(__FILE__) . '/../Data/tmp/test_thumb.jpg';
        $this->testImg = dirname(__FILE__) . '/../Data/test.jpg';

        parent::setUp();
    }

    public function tearDown()
    {
        if (file_exists($this->tmpTestImg)) {
            unlink($this->tmpTestImg);
        }

        parent::tearDown();
    }

    public function testGetId()
    {
        $attachment = new Entity\CvAttachment();
        $attachment->setId(1234);

        $this->assertEquals(1234, $attachment->getId());
    }

    public function testSetType()
    {
        $type = 'passphoto';
        $attachment = new Entity\CvAttachment();
        $attachment->setType($type);
        $this->assertEquals($type, $attachment->getType());
    }

    public function testSetUploaddate()
    {
        $uploadedDate = time();
        $attachment = new Entity\CvAttachment();

        // uploaded date is not set
        $this->assertNotEmpty($attachment->getUploaddate());

        // uploaded date is set
        $attachment->setUploaddate($uploadedDate);
        $this->assertEquals($uploadedDate, $attachment->getUploaddate());
    }

    public function testSetPath()
    {
        $path = '/tmp';
        $attachment = new Entity\CvAttachment();
        $attachment->setUploadName($path);
        $this->assertEquals($path, $attachment->getUploadName());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $attachment = new Entity\CvAttachment();
        $attachment->setSortposition($sortposition);
        $this->assertEquals($sortposition, $attachment->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $attachment = new Entity\CvAttachment();
        $attachment->setCv($cv);
        $this->assertEquals($cv, $attachment->getCv());
    }

    public function testPreUpload()
    {
        $attachment = new Entity\CvAttachment();

        $attachment->preUpload();
        $this->assertNull($attachment->getUploadName());

        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile($this->testImg, 'test.jpg');

        $attachment->setFile($file);
        $attachment->preUpload();
        $this->assertEquals($attachment->getUploadName(), 'test.jpg');
    }

    public function testUpload()
    {
        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $this->assertNull($attachment->upload());

        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;
        $cv = $this->entityManager->getRepository('CvCreatorBundle:Cv')->find($this->cvId);

        $attachment->setCv($cv);
        $attachment->upload();

        $this->assertTrue(file_exists($attachment->getAbsolutePath()));

    }

    public function testRemoveUpload()
    {
        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;

        $attachment->upload();

        $this->assertTrue(file_exists($attachment->getAbsolutePath()));

        $attachment->removeUpload();

        $this->assertFalse(file_exists($attachment->getAbsolutePath()));

    }

    public function testGetAbsolutePath()
    {
        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        /** @var $cv Entity\Cv */
        $cv = $this->entityManager->getRepository('CvCreatorBundle:Cv')->find($this->cvId);

        $attachment->setCv($cv);
        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;
        $attachment->setId(99999);

        $attachment->upload();

        $expectedPath = __DIR__
            . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . 'Resources'
            . DIRECTORY_SEPARATOR . 'storage'
            . DIRECTORY_SEPARATOR . $cv->getUser()->getId()
            . DIRECTORY_SEPARATOR . $cv->getId()
            . DIRECTORY_SEPARATOR . 'img'
            . DIRECTORY_SEPARATOR . $attachment->getId() . '.jpg';

        $this->assertEquals(realpath($expectedPath), realpath($attachment->getAbsolutePath()));
    }

    public function testGetWebPath()
    {
        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        /** @var $cv Entity\Cv */
        $cv = $this->entityManager->getRepository('CvCreatorBundle:Cv')->find($this->cvId);

        $attachment->setCv($cv);
        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;
        $attachment->setId(99999);

        $attachment->upload();

        $expectedPath = $cv->getUser()->getId()
            . DIRECTORY_SEPARATOR . $cv->getId()
            . DIRECTORY_SEPARATOR . 'img'
            . DIRECTORY_SEPARATOR . $attachment->getId() . '.jpg';

        $this->assertEquals($expectedPath, $attachment->getWebPath());
    }

    public function testGetMimeContentType()
    {
        $attachment = new Entity\CvAttachment();
        $this->assertEquals('application/pdf; charset=utf-8', $attachment->getMimeContentType('pdf'));
        $this->assertEquals('application/rtf', $attachment->getMimeContentType('rtf'));
        $this->assertEquals('application/msword', $attachment->getMimeContentType('doc'));
        $this->assertEquals('image/jpeg', $attachment->getMimeContentType('jpg'));
        $this->assertEquals('image/png', $attachment->getMimeContentType('png'));
        $this->assertEquals('image/bmp', $attachment->getMimeContentType('bmp'));
        $this->assertEquals('', $attachment->getMimeContentType('xxxx'));
        $this->assertEquals('', $attachment->getMimeContentType(''));
    }

    public function testGetFileName()
    {
        $attachment = new Entity\CvAttachment();
        $this->assertNull($attachment->getFileName());

        $attachment->setUploadName('/tmp/test.jpg');
        $this->assertEquals('test.jpg', $attachment->getFileName());
    }

    public function testGetThumb()
    {
        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;
        $attachment->setId(9999);

        $attachment->upload();

        $thumb = $attachment->getFileContent(true);

        $this->assertNotEmpty($thumb);

        $attachment->removeUpload();
    }

    public function testGetFileContent()
    {
        $testImgContent = file_get_contents($this->testImg);
        file_put_contents($this->tmpTestImg, $testImgContent);

        $attachment = new Entity\CvAttachment();
        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
            $this->tmpTestImg, 'test.jpg', null, null, null, true
        );

        $attachment->setUploadName('test.jpg');
        $attachment->file = $file;

        $attachment->upload();

        $fileContent = $attachment->getFileContent();

        $this->assertNotNull($fileContent);

        $fileContent = $attachment->getFileContent(true);

        $this->assertNotNull($fileContent);
    }

    public function testGetIcon()
    {
        $attachment = new Entity\CvAttachment();
        $this->assertNull($attachment->getIcon());
    }
}
