<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvCustomItemTest extends \PHPUnit_Framework_TestCase
{
    public function testGetId()
    {
        $item = new Entity\CvCustomItem();
        $this->assertEmpty($item->getId());
    }

    public function testSetTitle()
    {
        $title = "Sprachaufenthalt";
        $item = new Entity\CvCustomItem();
        $item->setTitle($title);
        $this->assertEquals($title, $item->getTitle());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $item = new Entity\CvCustomItem();
        $item->setSortposition($sortposition);
        $this->assertEquals($sortposition, $item->getSortposition());
    }

    public function testSetDescription()
    {
        $description = "Test section description";
        $item = new Entity\CvCustomItem();
        $item->setDescription($description);
        $this->assertEquals($description, $item->getDescription());
    }

    public function testSetSection()
    {
        $section = new Entity\CvSection();
        $item = new Entity\CvCustomItem();
        $item->setSection($section);
        $this->assertEquals($section, $item->getSection());
    }

    public function testSetCv()
    {
        $item = new Entity\CvCustomItem();
        $item->setCv(new Entity\Cv());
        $this->assertTrue($item->getCv() instanceof Entity\Cv);
    }
}