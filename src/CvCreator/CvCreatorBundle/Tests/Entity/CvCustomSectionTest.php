<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvSectionTest extends \PHPUnit_Framework_TestCase
{
    public function testGetId()
    {
        $section = new Entity\CvSection();
        $this->assertEmpty($section->getId());
    }

    public function testSetTitle()
    {
        $title = "Hobbys";
        $section = new Entity\CvSection();
        $section->setTitle($title);
        $this->assertEquals($title, $section->getTitle());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $section = new Entity\CvSection();
        $section->setSortposition($sortposition);
        $this->assertEquals($sortposition, $section->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $section = new Entity\CvSection();
        $section->setCv($cv);
        $this->assertEquals($cv, $section->getCv());
    }
}