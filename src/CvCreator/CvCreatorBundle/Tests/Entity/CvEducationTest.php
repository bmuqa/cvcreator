<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvEducationTest extends \PHPUnit_Framework_TestCase
{
    public function testSetId()
    {
        $id = 1234;
        $section = new Entity\CvEducation();
        $section->setId($id);
        $this->assertEquals($section->getId(), $id);
    }

    public function testSetDatefrom()
    {
        $date = time();
        $education = new Entity\CvEducation();
        $education->setDatefrom($date);
        $this->assertEquals($date, $education->getDatefrom());
    }

    public function testSetDateto()
    {
        $date = time();
        $education = new Entity\CvEducation();
        $education->setDateto($date);
        $this->assertEquals($date, $education->getDateto());
    }

    public function testSetTitle()
    {
        $title = 'Primarschule';
        $education = new Entity\CvEducation();
        $education->setTitle($title);
        $this->assertEquals($title, $education->getTitle());
    }

    public function testSetLevel()
    {
        $level = 'isced1';
        $education = new Entity\CvEducation();
        $education->setLevel($level);
        $this->assertEquals($level, $education->getLevel());
    }

    public function testSetSchool()
    {
        $school = 'Harvard';
        $education = new Entity\CvEducation();
        $education->setSchool($school);
        $this->assertEquals($school, $education->getSchool());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $education = new Entity\CvEducation();
        $education->setSortposition($sortposition);
        $this->assertEquals($sortposition, $education->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $education = new Entity\CvEducation();
        $education->setCv($cv);
        $this->assertEquals($cv, $education->getCv());
    }
}