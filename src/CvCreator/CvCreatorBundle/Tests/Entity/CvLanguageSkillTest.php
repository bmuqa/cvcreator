<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvLanguageSkillTest extends \PHPUnit_Framework_TestCase
{
    public function testSetId()
    {
        $id = 1234;
        $section = new Entity\CvLanguageSkill();
        $section->setId($id);
        $this->assertEquals($section->getId(), $id);
    }

    public function testSetLanguage()
    {
        $language = 'de';
        $languageSkill = new Entity\CvLanguageSkill();
        $languageSkill->setLanguage($language);
        $this->assertEquals($language, $languageSkill->getLanguage());
    }

    public function testSetRating()
    {
        $rating = 'de';
        $languageSkill = new Entity\CvLanguageSkill();
        $languageSkill->setRating($rating);
        $this->assertEquals($rating, $languageSkill->getRating());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $languageSkill = new Entity\CvLanguageSkill();
        $languageSkill->setSortposition($sortposition);
        $this->assertEquals($sortposition, $languageSkill->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $languageSkill = new Entity\CvLanguageSkill();
        $languageSkill->setCv($cv);
        $this->assertEquals($cv, $languageSkill->getCv());
    }
}