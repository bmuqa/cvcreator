<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvReferenceTest extends \PHPUnit_Framework_TestCase
{
    public function testSetId()
    {
        $id = 1234;
        $section = new Entity\CvReference();
        $section->setId($id);
        $this->assertEquals($section->getId(), $id);
    }

    public function testSetName()
    {
        $name = 'Hans Muster';
        $reference = new Entity\CvReference();
        $reference->setName($name);
        $this->assertEquals($name, $reference->getName());
    }

    public function testSetCompany()
    {
        $company = 'Microsoft';
        $reference = new Entity\CvReference();
        $reference->setCompany($company);
        $this->assertEquals($company, $reference->getCompany());
    }

    public function testSetContactdata()
    {
        $contactdata = 'Bahnhofstrasse 39, Zürich';
        $reference = new Entity\CvReference();
        $reference->setContactdata($contactdata);
        $this->assertEquals($contactdata, $reference->getContactdata());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $reference = new Entity\CvReference();
        $reference->setSortposition($sortposition);
        $this->assertEquals($sortposition, $reference->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $reference = new Entity\CvReference();
        $reference->setCv($cv);
        $this->assertEquals($cv, $reference->getCv());
    }
}