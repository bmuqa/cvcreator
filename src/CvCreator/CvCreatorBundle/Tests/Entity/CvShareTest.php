<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvShareTest extends \PHPUnit_Framework_TestCase
{
    public function testGetId()
    {
        $share = new Entity\CvShare();
        $this->assertEmpty($share->getId());
    }

    public function testSetEmail()
    {
        $email = 'hans.muster@gmail.com';
        $share = new Entity\CvShare();
        $share->setEmail($email);
        $this->assertEquals($email, $share->getEmail());
    }

    public function testSetDate()
    {
        $date = time();
        $share = new Entity\CvShare();
        $share->setDate($date);
        $this->assertEquals($date, $share->getDate());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $share = new Entity\CvShare();
        $share->setCv($cv);
        $this->assertEquals($cv, $share->getCv());
    }

    public function testSetUser(\CvCreator\CvCreatorBundle\Entity\User $user = null)
    {
        $user = new \CvCreator\CvCreatorBundle\Entity\User();
        $share = new Entity\CvShare();
        $share->setUser($user);
        $this->assertEquals($user, $share->getUser());
    }
}