<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Mapper;
use CvCreator\CvCreatorBundle\Entity;

class CvTest extends \PHPUnit_Framework_TestCase
{
    public function testGetId()
    {
        $cv = new Entity\Cv();
        $this->assertEmpty($cv->getId());
    }

    public function testSetId()
    {
        $id = 1234;
        $cv = new Entity\Cv();
        $cv->setId($id);
        $this->assertEquals($id, $cv->getId());
    }

    public function testSetCreatedate()
    {
        $date = time();
        $cv = new Entity\Cv();
        $cv->setCreatedate($date);
        $this->assertEquals($date, $cv->getCreatedate());
    }

    public function testSetUpdatedate()
    {
        $date = time();
        $cv = new Entity\Cv();
        $cv->setUpdatedate($date);
        $this->assertEquals($date, $cv->getUpdatedate());
    }

    public function testSetTitle()
    {
        $title = "test cv title";
        $cv = new Entity\Cv();
        $cv->setTitle($title);
        $this->assertEquals($title, $cv->getTitle());
    }

    public function testSetFormat()
    {
        $format = 3;
        $cv = new Entity\Cv();
        $cv->setFormat($format);
        $this->assertEquals($format, $cv->getFormat());
    }

    public function testSetLayout()
    {
        $layout = 3;
        $cv = new Entity\Cv();
        $cv->setLayout($layout);
        $this->assertEquals($layout, $cv->getLayout());
    }

    public function testSetUser()
    {
        $user = new \CvCreator\CvCreatorBundle\Entity\User();
        $cv = new Entity\Cv();
        $cv->setUser($user);
        $this->assertEquals($user, $cv->getUser());
    }
}