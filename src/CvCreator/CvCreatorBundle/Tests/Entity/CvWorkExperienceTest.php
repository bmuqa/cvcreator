<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvWorkExperienceTest extends \PHPUnit_Framework_TestCase
{
    public function testSetId()
    {
        $id = 1234;
        $section = new Entity\CvWorkExperience();
        $section->setId($id);
        $this->assertEquals($section->getId(), $id);
    }

    public function testSetDatefrom()
    {
        $date = time();
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setDatefrom($date);
        $this->assertEquals($date, $workExperience->getDatefrom());
    }

    public function testSetDateto()
    {
        $date = time();
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setDateto($date);
        $this->assertEquals($date, $workExperience->getDateto());
    }

    public function testSetStillworking()
    {
        $stillWorking = true;
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setStillworking($stillWorking);
        $this->assertEquals($stillWorking, $workExperience->getStillworking());
    }

    public function testSetProfession()
    {
        $profession = "Maler";
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setProfession($profession);
        $this->assertEquals($profession, $workExperience->getProfession());
    }

    public function testSetEmployment()
    {
        $employment = "Festanstellung";
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setEmployment($employment);
        $this->assertEquals($employment, $workExperience->getEmployment());
    }

    public function testSetCompany()
    {
        $company = "Google";
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setCompany($company);
        $this->assertEquals($company, $workExperience->getCompany());
    }

    public function testSetActivities()
    {
        $activities = "Malen, tappezieren, etc.";
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setActivities($activities);
        $this->assertEquals($activities, $workExperience->getActivities());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setSortposition($sortposition);
        $this->assertEquals($sortposition, $workExperience->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $workExperience = new Entity\CvWorkExperience();
        $workExperience->setCv($cv);
        $this->assertEquals($cv, $workExperience->getCv());
    }
}