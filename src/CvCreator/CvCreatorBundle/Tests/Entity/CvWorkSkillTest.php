<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class CvWorkSkillTest extends \PHPUnit_Framework_TestCase
{
    public function testSetId()
    {
        $id = 1234;
        $section = new Entity\CvWorkSkill();
        $section->setId($id);
        $this->assertEquals($section->getId(), $id);
    }

    public function testSetSkill()
    {
        $skill = 'malen';
        $workSkill = new Entity\CvWorkSkill();
        $workSkill->setSkill($skill);
        $this->assertEquals($skill, $workSkill->getSkill());
    }

    public function testSetRating()
    {
        $rating = 5;
        $workSkill = new Entity\CvWorkSkill();
        $workSkill->setRating($rating);
        $this->assertEquals($rating, $workSkill->getRating());
    }

    public function testSetSortposition()
    {
        $sortposition = 3;
        $workSkill = new Entity\CvWorkSkill();
        $workSkill->setSortposition($sortposition);
        $this->assertEquals($sortposition, $workSkill->getSortposition());
    }

    public function testSetCv()
    {
        $cv = new \CvCreator\CvCreatorBundle\Entity\Cv();
        $workSkill = new Entity\CvWorkSkill();
        $workSkill->setCv($cv);
        $this->assertEquals($cv, $workSkill->getCv());
    }
}