<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

require_once dirname(__DIR__).'/../../../../app/AppKernel.php';

class PersonalDataTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\HttpKernel\AppKernel
     */
    protected $kernel;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    protected $cvId = 51;

    protected $tmpPassPhoto;


    public function setUp()
    {
        // Boot the AppKernel in the test environment and with the debug.
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        // Store the container and the entity manager in test case properties
        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();

        parent::setUp();
    }

    public function tearDown()
    {
        // Shutdown the kernel.
        $this->kernel->shutdown();

        if (file_exists($this->tmpPassPhoto)) {
            unlink($this->tmpPassPhoto);
        }

        parent::tearDown();
    }

    public function testGetId()
    {
        $id = 12345;
        $personalData = new Entity\CvPersonalData();
        $personalData->setId($id);
        $this->assertEquals($id, $personalData->getId());
    }

    public function testSetGender()
    {
        $gender = 'Male';
        $personalData = new Entity\CvPersonalData();
        $personalData->setGender($gender);
        $this->assertEquals($gender, $personalData->getGender());
    }

    public function testSetFirstname()
    {
        $firstname = 'Male';
        $personalData = new Entity\CvPersonalData();
        $personalData->setFirstname($firstname);
        $this->assertEquals($firstname, $personalData->getFirstname());
    }

    public function testSetLastname()
    {
        $lastname = 'Male';
        $personalData = new Entity\CvPersonalData();
        $personalData->setLastname($lastname);
        $this->assertEquals($lastname, $personalData->getLastname());
    }

    public function testSetBirthday()
    {
        $birthday = '1979-10-12';
        $personalData = new Entity\CvPersonalData();
        $personalData->setBirthday($birthday);
        $this->assertEquals($birthday, $personalData->getBirthday());
    }

    public function testSetMartialstatus()
    {
        $martialstatus = 'married';
        $personalData = new Entity\CvPersonalData();
        $personalData->setMartialstatus($martialstatus);
        $this->assertEquals($martialstatus, $personalData->getMartialstatus());
    }

    public function testSetEmail()
    {
        $email = 'hans.muster@gmail.com';
        $personalData = new Entity\CvPersonalData();
        $personalData->setEmail($email);
        $this->assertEquals($email, $personalData->getEmail());
    }

    public function testSetAddress()
    {
        $zip = '8005';
        $personalData = new Entity\CvPersonalData();
        $personalData->setAddress($zip);
        $this->assertEquals($zip, $personalData->getAddress());
    }

    public function testSetZip()
    {
        $zip = '8005';
        $personalData = new Entity\CvPersonalData();
        $personalData->setZip($zip);
        $this->assertEquals($zip, $personalData->getZip());
    }

    public function testSetCity()
    {
        $city = 'Zürich';
        $personalData = new Entity\CvPersonalData();
        $personalData->setCity($city);
        $this->assertEquals($city, $personalData->getCity());
    }

    public function testSetCountry()
    {
        $country = 'Switzerland';
        $personalData = new Entity\CvPersonalData();
        $personalData->setCountry($country);
        $this->assertEquals($country, $personalData->getCountry());
    }

    public function testSetPhone()
    {
        $country = 'Switzerland';
        $personalData = new Entity\CvPersonalData();
        $personalData->setPhone($country);
        $this->assertEquals($country, $personalData->getPhone());
    }

    public function testSetMobile()
    {
        $mobile = '07123456789';
        $personalData = new Entity\CvPersonalData();
        $personalData->setMobile($mobile);
        $this->assertEquals($mobile, $personalData->getMobile());
    }

    public function testSetFax()
    {
        $fax = '07123456789';
        $personalData = new Entity\CvPersonalData();
        $personalData->setFax($fax);
        $this->assertEquals($fax, $personalData->getFax());
    }

    public function testSetWeb()
    {
        $web = '07123456789';
        $personalData = new Entity\CvPersonalData();
        $personalData->setWeb($web);
        $this->assertEquals($web, $personalData->getWeb());
    }

    public function testSetAddcontactdata()
    {
        $addcontactdata = 'facebook: burim_m@hotmail.com';
        $personalData = new Entity\CvPersonalData();
        $personalData->setAddcontactdata($addcontactdata);
        $this->assertEquals($addcontactdata, $personalData->getAddcontactdata());
    }

    public function testSetCreatedate()
    {
        $date = time();
        $personalData = new Entity\CvPersonalData();
        $personalData->setCreatedate($date);
        $this->assertEquals($date, $personalData->getCreatedate());
    }

    public function testSetUpdatedate()
    {
        $date = time();
        $personalData = new Entity\CvPersonalData();
        $personalData->setUpdatedate($date);
        $this->assertEquals($date, $personalData->getUpdatedate());
    }

    public function testSetPassphoto()
    {
        $passphoto = '/tmp/test';
        $personalData = new Entity\CvPersonalData();
        $personalData->setPassphoto($passphoto);
        $this->assertEquals($passphoto, $personalData->getPassphoto());
    }

    public function testSetNationality()
    {
        $nationality = 'German';
        $personalData = new Entity\CvPersonalData();
        $personalData->setNationality($nationality);
        $this->assertEquals($nationality, $personalData->getNationality());
    }

    public function testSetSecondNationality()
    {
        $secondNationality = 'German';
        $personalData = new Entity\CvPersonalData();
        $personalData->setSecondNationality($secondNationality);
        $this->assertEquals($secondNationality, $personalData->getSecondNationality());
    }

    public function testSetDriverLicense()
    {
        $driverLicense = 'B,A,C';
        $personalData = new Entity\CvPersonalData();
        $personalData->setDriverLicense($driverLicense);
        $this->assertEquals(array('B', 'A', 'C'), $personalData->getDriverLicense());
    }

    public function testGetNoticePeriod()
    {
        $noticePeriod = '3 Monate';
        $personalData = new Entity\CvPersonalData();
        $personalData->setNoticePeriod($noticePeriod);
        $this->assertEquals($noticePeriod, $personalData->getNoticePeriod());
    }

    public function testGetHobbies()
    {
        $hobbies = 'Volleyball, Fussball';
        $personalData = new Entity\CvPersonalData();
        $personalData->setHobbies($hobbies);
        $this->assertEquals($hobbies, $personalData->getHobbies());
    }

    public function testGetSortPosition()
    {
        $sortPosition = 3;
        $personalData = new Entity\CvPersonalData();
        $personalData->setSortposition($sortPosition);
        $this->assertEquals(1, $personalData->getSortPosition());
    }

    public function testGetCv()
    {
        $cv = new Entity\Cv();
        $personalData = new Entity\CvPersonalData();
        $personalData->setCv($cv);
        $this->assertEquals($cv, $personalData->getCv());
    }

    public function testPreparePassPhoto()
    {
        $this->tmpPassPhoto = dirname(__FILE__) . '/../../Resources/storage/test.jpg';
        $personalData = new Entity\CvPersonalData();

        /** @var $cv Entity\Cv */
        $cv = $this->entityManager->getRepository('CvCreatorBundle:Cv')->find($this->cvId);

        file_put_contents($this->tmpPassPhoto, 'dummy content');

        $passPhoto = new \Symfony\Component\HttpFoundation\File\UploadedFile($this->tmpPassPhoto, 'test.jpg');

        $personalData->preparePassPhoto($cv, $this->entityManager);

        // passphoto is empty
        $this->assertTrue($personalData->getPassphoto() instanceof Entity\CvAttachment);

        $personalData->setPassphoto($passPhoto);
        $personalData->preparePassPhoto($cv, $this->entityManager);

        // passphoto is set
        $this->assertTrue($personalData->getPassphoto() instanceof Entity\CvAttachment);

        $user = new Entity\User();
        $user->setId(88888888);

        $newCv = new Entity\Cv();
        $newCv->setId(9999999);
        $newCv->setUser($user);

        $personalData->preparePassPhoto($newCv, $this->entityManager);

        // no passphoto is into the db
        $this->assertTrue($personalData->getPassphoto() instanceof Entity\CvAttachment);
    }
}