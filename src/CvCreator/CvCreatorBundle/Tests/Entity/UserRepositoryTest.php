<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

require_once dirname(__DIR__).'/../../../../app/AppKernel.php';

class UserRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\HttpKernel\AppKernel
     */
    protected $kernel;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;


    public function setUp()
    {
        // Boot the AppKernel in the test environment and with the debug.
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        // Store the container and the entity manager in test case properties
        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();

        parent::setUp();
    }

    public function tearDown()
    {
        // Shutdown the kernel.
        $this->kernel->shutdown();

        parent::tearDown();
    }


    /**
     * @expectedException \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     */
    public function testLoadUserByUsername()
    {
        $classMapp = new \Doctrine\ORM\Mapping\ClassMetadata('CvCreator\CvCreatorBundle\Entity\User');
        $userRepository = new Entity\UserRepository($this->entityManager, $classMapp);

        $user = $userRepository->loadUserByUsername('bmuqa');
        $this->assertEquals($user->getUsername(), 'bmuqa');


        $user = $userRepository->loadUserByUsername('bmuqa' . time());
        $this->assertNull($user->getUsername());

    }

    public function testRefreshUser()
    {
        $classMapp = new \Doctrine\ORM\Mapping\ClassMetadata('CvCreator\CvCreatorBundle\Entity\User');
        $userRepository = new Entity\UserRepository($this->entityManager, $classMapp);

        /** @var $user Entity\User */
        $user = $this->entityManager->getRepository('CvCreatorBundle:User')->find(259);

        $resultUser = $userRepository->refreshUser($user);
        $this->assertEquals($resultUser->getUsername(), $user->getUsername());
    }

    public function testSupportsClass()
    {
        $classMapp = new \Doctrine\ORM\Mapping\ClassMetadata('CvCreator\CvCreatorBundle\Entity\User');
        $userRepository = new Entity\UserRepository($this->entityManager, $classMapp);

        $class = 'CvCreator\CvCreatorBundle\Entity\User';

        $this->assertTrue($userRepository->supportsClass($class));
    }
}