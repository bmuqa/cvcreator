<?php
namespace CvCreator\CvCreatorBundle\Tests\Entity;

use CvCreator\CvCreatorBundle\Entity;

class UserTest extends \PHPUnit_Framework_TestCase
{
    public function testSetId()
    {
        $id = 1234;
        $section = new Entity\User();
        $section->setId($id);
        $this->assertEquals($section->getId(), $id);
    }

    public function testSetGender()
    {
        $gender = 'Male';
        $user = new Entity\User();
        $user->setGender($gender);
        $this->assertEquals($gender, $user->getGender());
    }

    public function testSetFirstname()
    {
        $firstname = 'Male';
        $user = new Entity\User();
        $user->setFirstname($firstname);
        $this->assertEquals($firstname, $user->getFirstname());
    }

    public function testSetLastname()
    {
        $lastname = 'Male';
        $user = new Entity\User();
        $user->setLastname($lastname);
        $this->assertEquals($lastname, $user->getLastname());
    }

    public function testSetBirthday()
    {
        $birthday = '1979-10-12';
        $user = new Entity\User();
        $user->setBirthday($birthday);
        $this->assertEquals($birthday, $user->getBirthday());
    }

    public function testSetEmail()
    {
        $email = 'hans.muster@gmail.com';
        $user = new Entity\User();
        $user->setEmail($email);
        $this->assertEquals($email, $user->getEmail());
    }

    public function testSetUsername()
    {
        $username = 'hans.muster';
        $user = new Entity\User();
        $user->setUsername($username);
        $this->assertEquals($username, $user->getUsername());
    }

    public function testSetPassword()
    {
        $password = '123456';
        $user = new Entity\User();
        $user->setPassword($password);
        $this->assertEquals($password, $user->getPassword());
    }

    public function testSetCreatedate()
    {
        $date = time();
        $user = new Entity\User();
        $user->setCreatedate($date);
        $this->assertEquals($date, $user->getCreatedate());
    }

    public function testSetUpdatedate()
    {
        $date = time();
        $user = new Entity\User();
        $user->setUpdatedate($date);
        $this->assertEquals($date, $user->getUpdatedate());
    }

    public function testSetIsactive()
    {
        $isactive = true;
        $user = new Entity\User();
        $user->setIsactive($isactive);
        $this->assertEquals($isactive, $user->getIsactive());
    }

    public function testSetConfirmcode()
    {
        $confirmcode = 'fsd12sdfsdf2s1dfsdf235';
        $user = new Entity\User();
        $user->setConfirmcode($confirmcode);
        $this->assertEquals($confirmcode, $user->getConfirmcode());
    }

    public function testSetLoginattempt()
    {
        $loginattempt = 3;
        $user = new Entity\User();
        $user->setLoginattempt($loginattempt);
        $this->assertEquals($loginattempt, $user->getLoginattempt());
    }

    public function testSetResetpasswordcode()
    {
        $resetpasswordcode = 'sdf4s4f5f4s6f48f97sdf6d4';
        $user = new Entity\User();
        $user->setResetpasswordcode($resetpasswordcode);
        $this->assertEquals($resetpasswordcode, $user->getResetpasswordcode());
    }

    public function testGetRoles()
    {
        $roles = array('ROLE_USER');
        $user = new Entity\User();
        $this->assertEquals($roles, $user->getRoles());
    }

    public function testSetSalt()
    {
        $salt = 'sdf4s4f5f4s6f48f97sdf6d4';
        $user = new Entity\User();
        $user->setSalt($salt);
        $this->assertEquals($salt, $user->getSalt());
    }

    public function testEquals()
    {
        $user = new Entity\User();
        $user1 = new Entity\User();
        $this->assertTrue($user->equals($user1));
        $user1->setUsername('Hans');
        $this->assertFalse($user->equals($user1));
    }

    public function testSerialize()
    {
        $user = new Entity\User();
        $user->setPassword('123456');
        $user->setUsername('hans.muster');
        $serialized = $user->serialize();
        $this->assertEquals(serialize(array(null, '123456', 'hans.muster')), $serialized);
    }

    public function testUnserialize()
    {
        $user = new Entity\User();
        $user->setPassword('123456');
        $user->setUsername('hans.muster');
        $user1 = new Entity\User();
        $user1->unserialize(serialize(array(null, '123456', 'hans.muster')));
        $this->assertEquals($user, $user1);
    }


    public function testIsAccountNonExpired()
    {
        $user = new Entity\User();
        $this->assertTrue($user->isAccountNonExpired());
    }

    public function testIsAccountNonLocked()
    {
        $user = new Entity\User();
        $this->assertTrue($user->isAccountNonLocked());
    }

    public function testIsCredentialsNonExpired()
    {
        $user = new Entity\User();
        $this->assertTrue($user->isCredentialsNonExpired());
    }

    public function testIsEnabled()
    {
        $user = new Entity\User();
        $this->assertTrue($user->isEnabled());
    }

    public function testGetSortPosition()
    {
        $user = new Entity\User();
        $this->assertEquals($user->getSortPosition(), 1);
    }

    public function testGetCv()
    {
        $user = new Entity\User();
        $this->assertTrue($user->getCv());
    }

    public function testSetCv()
    {
        $user = new Entity\User();
        $this->assertTrue($user->setCv(new Entity\Cv()) instanceof Entity\User);
    }

    public function testEraseCredentials()
    {
        $user = new Entity\User();
        $this->assertEmpty($user->eraseCredentials());
    }

    public function testRemoveAllFiles()
    {
        $user = new Entity\User();
        $user->setId(9999999);

        $this->assertFalse($user->removeAllFiles('', ''));

        $storagePath = realpath(dirname(dirname(dirname(__FILE__))))
            . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'storage';

        $user_dir = $storagePath . DIRECTORY_SEPARATOR . $user->getId();

        if (is_dir($user_dir)) {
            $user->removeAllFiles($storagePath);
        }

        mkdir($user_dir);
        $this->assertTrue(is_dir($user_dir));

        mkdir($user_dir . DIRECTORY_SEPARATOR . 'pdf');
        $this->assertTrue(is_dir($user_dir . DIRECTORY_SEPARATOR . 'pdf'));

        $ourFileName = $user_dir . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . 'test.txt';
        $fileHandle = fopen($ourFileName, 'w') or die("can't create new file");
        fclose($fileHandle);

        $this->assertTrue(file_exists($ourFileName));

        $user->removeAllFiles($storagePath);
        $this->assertTrue(!is_dir($user_dir));
    }

}