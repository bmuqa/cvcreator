<?php
namespace CvCreator\CvCreatorBundle\Tests\Mapper;

use CvCreator\CvCreatorBundle\Mapper;
use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Form\Type;

require_once dirname(__DIR__).'/../../../../app/AppKernel.php';

class CvMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\HttpKernel\AppKernel
     */
    protected $kernel;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;


    protected $cv_id = 52;


    public function setUp()
    {
        // Boot the AppKernel in the test environment and with the debug.
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        // Store the container and the entity manager in test case properties
        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();

        parent::setUp();
    }

    public function tearDown()
    {
        // Shutdown the kernel.
        $this->kernel->shutdown();

        parent::tearDown();
    }

    public function testGetSections()
    {
        $cv = $this->entityManager->getRepository('CvCreatorBundle:Cv')->find($this->cv_id);
        $cvMapper = new Mapper\CvMapper($this->entityManager, $this->container->get('translator'), $cv);
        $sections = $cvMapper->getSections();

        $this->assertEquals(count($sections), 7);
    }

    public function testGetSection()
    {
        $cv = $this->entityManager->getRepository('CvCreatorBundle:Cv')->find($this->cv_id);
        $cvMapper = new Mapper\CvMapper($this->entityManager, $this->container->get('translator'), $cv);

        $this->assertNotEmpty($cvMapper->getSection('personaldata'));
    }
}