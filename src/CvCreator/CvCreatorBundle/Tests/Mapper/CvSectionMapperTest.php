<?php
namespace CvCreator\CvCreatorBundle\Tests\Mapper;

use CvCreator\CvCreatorBundle\Mapper;
use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Form\Type;
use Symfony\Component\Form;

class CvSectionMapperTest extends \PHPUnit_Framework_TestCase
{
    public function testGetItems()
    {
        $entity = new Entity\CvPersonalData();
        $type = new Type\PersonalDataType();
        $items = null;

        $cvSection = new Mapper\CvSectionMapper($type, 'Persönliche Daten', new Entity\Cv(), false, false);
        $this->assertEquals(count($cvSection->getItems()), 0);

        $cvSection = new Mapper\CvSectionMapper($type, 'Persönliche Daten', new Entity\Cv(), false, false);
        $cvSection->addItem($entity);
        $this->assertEquals(1, count($cvSection->getItems()));
    }

    public function testGetItem()
    {
        $entity = new Entity\CvPersonalData();
        $entity->setId(1234);
        $type = new Type\PersonalDataType();

        $sectionEntity = new Entity\CvSection();

        $cvSection = new Mapper\CvSectionMapper($type, new Entity\Cv(), $sectionEntity, false, false);
        $this->assertEmpty($cvSection->getItem(1234));

        $cvSection = new Mapper\CvSectionMapper($type, new Entity\Cv(), $sectionEntity, false, false);
        $cvSection->addItem($entity);
        $this->assertEquals($entity, $cvSection->getItem(1234));
    }

    public function testGetTitle()
    {
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten', new Entity\Cv(), false, false);
        $this->assertEquals('Persönliche Daten', $cvSection->getTitle());
    }

    public function testIsMultiple()
    {
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), true, true);
        $this->assertEquals(true, $cvSection->isMultiple());

        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), false, false);
        $this->assertEquals(false, $cvSection->isMultiple());
    }

    public function testGetFormView()
    {
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), false, false);
        $this->assertTrue($cvSection->getFormView() instanceof Form\FormView);

        $formView = new Form\FormView();
        $cvSection->setFormView($formView);

        $this->assertEquals($formView, $cvSection->getFormView());
    }

    public function testGetType()
    {
        $type = new Type\PersonalDataType();
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), null, false);
        $this->assertEquals($type, $cvSection->getType());
    }

    public function testGetEntity()
    {
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), null, false);
        $this->assertTrue($cvSection->getEntity() instanceof Entity\CvPersonalData);
    }

    public function testGetFormName()
    {
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), null, false);
        $formName = $cvSection->getFormName();

        $this->assertEquals($formName, 'personaldata');
    }

    public function testIsMovable()
    {
        $cvSection = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), null, true);
        $this->assertTrue($cvSection->isMovable());

        $cvSection1 = new Mapper\CvSectionMapper(new Type\PersonalDataType(), 'Persönliche Daten',  new Entity\Cv(), null, false);
        $this->assertFalse($cvSection1->isMovable());
    }

    public function testGetForm()
    {
        $form = new Type\PersonalDataType();
        $cvSection = new Mapper\CvSectionMapper($form, 'Persönliche Daten',  new Entity\Cv(), null, true);

        $this->assertEquals($form, $cvSection->getForm());

    }
}