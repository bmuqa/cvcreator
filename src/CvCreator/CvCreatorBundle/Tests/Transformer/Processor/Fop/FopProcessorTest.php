<?php
/**
 * Created by: Burim on 2013-02-19 20:02
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Processor\Fop;

use CvCreator\CvCreatorBundle\Mapper;
use CvCreator\CvCreatorBundle\Entity;
use CvCreator\CvCreatorBundle\Form\Type;

require_once dirname(__DIR__) . '/../../../../../../app/AppKernel.php';

class FopProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\HttpKernel\AppKernel
     */
    protected $kernel;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;


    protected $cv_id = 52;
    protected $user_id = 259;
    protected $userDir;
    protected $dataPath;
    protected $stylesheetPath;

    public function setUp()
    {
        $this->userDir = $this->user_id;
        $this->dataPath = dirname(__FILE__) . '/../../../Data';
        $this->stylesheetPath = dirname(__FILE__) . '/../../../../Resources/xsl';

        // Boot the AppKernel in the test environment and with the debug.
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        // Store the container and the entity manager in test case properties
        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();

        parent::setUp();
    }

    public function tearDown()
    {
        // Shutdown the kernel.
        $this->kernel->shutdown();

        if (file_exists($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.pdf')) {
            unlink($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.pdf');
        }

        if (file_exists($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.log')) {
            unlink($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.log');
        }

        parent::tearDown();
    }

    /**
     * @expectedException \CvCreator\CvCreatorBundle\Transformer\Processor\ProcessorException
     */
    public function testTransform()
    {


        /** @var $source \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface */
        $source = new \CvCreator\CvCreatorBundle\Transformer\Source\Xhtml($this->dataPath);
        $source->setUserDir($this->userDir)
               ->setFileName('transform_source');

        $fop = new FopProcessor('/usr/local/fop/latest/fop');

        /** @var $output \CvCreator\CvCreatorBundle\Transformer\Target\TargetInterface */
        $target = new \CvCreator\CvCreatorBundle\Transformer\Target\Pdf($fop, $this->dataPath, '', $this->stylesheetPath);
        $target->setUserDir($this->userDir)
            ->setFileName('transform_target');

        $fop->transform($source, $target, 'standard');

        $this->assertTrue(file_exists($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.pdf'));

        if (file_exists($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.pdf')) {
            unlink($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.pdf');
        }

        if (file_exists($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.log')) {
            unlink($this->dataPath . '/' . $this->userDir . '/pdf/transform_target.log');
        }

        $fop = new FopProcessor('/usr/local/fop/latest/fop_not_exists');

        try {
            $fop->transform($source, $target, 'standard');
        } catch (InvalidArgumentException $expected) {
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }
}
