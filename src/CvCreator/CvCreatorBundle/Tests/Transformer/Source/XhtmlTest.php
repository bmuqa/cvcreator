<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-26 20:10
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use CvCreator\CvCreatorBundle\Transformer\Source;

/**
 * Class description:
 *
 * @author Burim
 */
class XhtmlTest extends \PHPUnit_Framework_TestCase
{
    public function tearDown()
    {

    }

    public function testGetPath()
    {
        $ds = DIRECTORY_SEPARATOR;
        $tempPath = dirname(__FILE__) . "{$ds}..{$ds}..{$ds}..{$ds}Resources{$ds}storage";
        $xhtml = new Source\Xhtml($tempPath);
        $filename = 'xyz';
        $xhtml->setFileName($filename);
        $path = $xhtml->getPath();

        $this->assertEquals($path, $tempPath . $ds . 'unknown' . $ds . 'xhtml' . $ds . 'xyz.xml');

        $userdir = '1234';
        $xhtml->setUserDir($userdir);
        $path = $xhtml->getPath();

        $this->assertEquals($path, $tempPath . $ds . '1234' . $ds . 'xhtml' . $ds . 'xyz.xml');
    }

    public function testSave()
    {
        $ds = DIRECTORY_SEPARATOR;
        $xhtml = new Source\Xhtml(dirname(__FILE__) . "{$ds}..{$ds}..{$ds}..{$ds}Resources{$ds}storage");
        $html = "<html><body><h1>das ist ein Test</h1></body></html>";
        $this->assertTrue($xhtml->save($html));
    }
}
