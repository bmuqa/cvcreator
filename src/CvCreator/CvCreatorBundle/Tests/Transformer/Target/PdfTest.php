<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-26 20:10
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use CvCreator\CvCreatorBundle\Transformer\Processor\Fop;
use CvCreator\CvCreatorBundle\Transformer\Target;

/**
 * Class description:
 *
 * @author Burim
 */
class PdfTest extends \PHPUnit_Framework_TestCase
{
    /** @var Target\Pdf */
    private $object;
    private $config;
    private $stylesheet;
    private $storage;

    protected function setUp()
    {
        $ds = DIRECTORY_SEPARATOR;
        $this->config = dirname(__FILE__) . "{$ds}..{$ds}..{$ds}..{$ds}Resources{$ds}config";
        $this->stylesheet = dirname(__FILE__) . "{$ds}..{$ds}..{$ds}..{$ds}Resources{$ds}xsl";
        $fopExecute = "fop";
        $this->storage = dirname(__FILE__) . "{$ds}..{$ds}..{$ds}..{$ds}Resources{$ds}storage";

        $this->object = new Target\Pdf(
            new Fop\FopProcessor($fopExecute),
            $this->storage,
            $this->config,
            $this->stylesheet
        );

        $this->object->setUserDir('test')
                     ->setFileName('test');
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testGetFormat()
    {
        $this->assertEquals('pdf', $this->object->getFormat());
    }

    public function testGetMimeContentType()
    {
        $this->assertEquals('application/pdf; charset=utf-8', $this->object->getMimeContentType());
    }

    public function testGetConfig()
    {
        $expect = ''; // '-c "' . $this->config . DIRECTORY_SEPARATOR . 'fop-pdf.xconf"';
        $this->assertEquals($expect, $this->object->getConfig());
    }

    public function testGetStylesheet()
    {
        $expect = $this->stylesheet . DIRECTORY_SEPARATOR . 'standard.xsl';
        $this->assertEquals($expect, $this->object->getStylesheet('standard'));
    }

    public function testGetContent()
    {
        $source = new \CvCreator\CvCreatorBundle\Transformer\Source\Xhtml($this->storage);
        $source->setUserDir('test')
               ->setFileName('test');
        $content = $this->object->getContent($source, 'standard');
        $this->assertNotEmpty($content);
    }

    public function testGetPath()
    {
        $this->object->setFileName('test');
        $expect = $this->storage . DIRECTORY_SEPARATOR . 'test' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . 'test.pdf';
        $this->assertEquals($expect, $this->object->getPath());

        if (is_dir($this->storage . DIRECTORY_SEPARATOR . '123456')) {
            @rmdir($this->storage . DIRECTORY_SEPARATOR . '123456');
        }

        $this->object->setUserDir('123456');
        $expect = $this->storage . DIRECTORY_SEPARATOR . '123456' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . 'test.pdf';
        $this->assertEquals($expect, $this->object->getPath());
    }
}
