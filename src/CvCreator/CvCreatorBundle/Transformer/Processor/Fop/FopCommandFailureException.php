<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-09 01:20
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Processor\Fop;

/**
 * Class description:
 *
 * @author Burim
 */
class FopCommandFailureException extends \Exception
{

}
