<?php
/**
 * Created by: Burim on 2012-09-08 00:32
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Processor\Fop;

/**
 * This class communicate with Apache FOP
 *
 * @author Burim
 */
class FopProcessor implements \CvCreator\CvCreatorBundle\Transformer\Processor\ProcessorInterface
{
    private $fopExecute;

    public function __construct($fopExecute)
    {
        $this->fopExecute = $fopExecute;
    }

    /**
     *
     * Command example:
     * fop -xml ...\temp\cv.html -xsl ...\public\xsl\xhtml2fo.xsl -pdf ...\temp\cv.pdf 2 > &1
     *
     * @param $outputFormat
     * @param $xslFile
     * @param $dataFile
     * @param $outputFileName
     * @return string
     */
    public function transform(
        \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface $source,
        \CvCreator\CvCreatorBundle\Transformer\Target\TargetInterface $target,
        $layout
    ) {
        // generate the output file with Apache FOP
        $shellCommand = $this->fopExecute . ' '
            . $target->getConfig()
            . ' -xml "' . $source->getPath() . '"'
            . ' -xsl "'. $target->getStylesheet($layout) .'"'
            . ' -' . $target->getFormat()
            . ' "' . $target->getPath() .'"'
            . ' 2> "' . $target->getPath() . '.log"';

        $result = shell_exec($shellCommand);

        if (!file_exists($target->getPath())) {
            throw new \CvCreator\CvCreatorBundle\Transformer\Processor\ProcessorException(
                "Generation process failed with output: " . file_get_contents($target->getPath() . '.log')
            );
        }
    }
}
