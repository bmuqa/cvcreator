<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-09-09 13:41
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Processor;

/**
 * Interface description:
 *
 * @author Burim
 */
interface ProcessorInterface
{
    public function transform(
        \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface $source,
        \CvCreator\CvCreatorBundle\Transformer\Target\TargetInterface $target,
        $layout
    );
}
