<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-23 21:32
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Source;

/**
 * Interface description:
 *
 * @author Burim
 */
interface SourceInterface
{
    public function getPath();
    public function setUserDir($userDir);
    public function setFileName($fileName);
    public function save($html, $options = null);
}
