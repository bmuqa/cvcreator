<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-18 14:58
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Source;

/**
 * Class description:
 *
 * @author Burim
 */
/**
 * Class Xhtml
 * @package CvCreator\CvCreatorBundle\Transformer\Source
 */
class Xhtml extends \DomDocument implements SourceInterface
{
    /**
     * @var
     */
    private $storagePath;
    /**
     * @var string
     */
    private $userDir;
    /**
     * @var string
     */
    private $fileName;

    /**
     * @param $storagePath
     */
    public function __construct($storagePath)
    {
        $this->storagePath = $storagePath;
        $this->userDir = 'unknown';
        $this->fileName = md5(time());
        parent::__construct('1.0', 'utf-8');
    }

    /**
     * @param $userDir
     * @return $this
     */
    public function setUserDir($userDir)
    {
        $this->userDir = $userDir;
        return $this;
    }

    /**
     * @param $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $storagePath = $this->storagePath . DIRECTORY_SEPARATOR . $this->userDir . DIRECTORY_SEPARATOR . 'xhtml';
        if (!is_dir($storagePath)) {
            mkdir($storagePath, 0777, true);
        }
        return $storagePath . DIRECTORY_SEPARATOR . $this->fileName . '.xml';
    }

    /**
     * Clean the Xhtml files dir
     */
    public function cleanDirectory()
    {
        $filesDirectoryPath = $storagePath = $this->storagePath
            . DIRECTORY_SEPARATOR . $this->userDir
            . DIRECTORY_SEPARATOR . 'xhtml'
            . DIRECTORY_SEPARATOR . '*';

        array_map('unlink', glob($filesDirectoryPath));
    }

    /**
     * @param string $html
     * @param null $options
     * @return bool
     * @throws SourceFileNotFoundException
     */
    public function save($html, $options = null)
    {
        $file = $this->getPath();
        if (!file_exists($file)) {
            $this->loadXML($html);
            // cleanup before save
            $this->cleanDirectory();
            parent::save($file);
        }

        if (!file_exists($file)) {
            throw new SourceFileNotFoundException('Source file not found!');
        }

        return true;
    }
}
