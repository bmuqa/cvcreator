<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-19 20:09
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Target;

/**
 * Class description:
 *
 * @author Burim
 */
class Pdf extends TargetAbstract implements TargetInterface
{
    /**
     * @param \CvCreator\CvCreatorBundle\Transformer\Processor\ProcessorInterface $processor
     * @param $storagePath
     * @param $fopConfigPath
     * @param $styleSheetPath
     */
    public function __construct(
        \CvCreator\CvCreatorBundle\Transformer\Processor\ProcessorInterface $processor,
        $storagePath,
        $fopConfigPath,
        $styleSheetPath
    ) {
        $this->storagePath = $storagePath;
        $this->processor = $processor;
        $this->fopConfigPath = $fopConfigPath;
        $this->stylesheetPath = $styleSheetPath;
        $this->userDir = 'unknown';
        $this->filename = md5(time()) . '.' . $this->getFormat();
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return 'pdf';
    }

    /**
     * @return string
     */
    public function getMimeContentType()
    {
        return 'application/pdf; charset=utf-8';
    }

    /**
     * @return string
     */
    public function getConfig()
    {
        return '-c "' . $this->fopConfigPath . DIRECTORY_SEPARATOR . 'fop-pdf.xconf"';
    }
}
