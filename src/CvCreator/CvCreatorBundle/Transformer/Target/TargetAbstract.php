<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-19 20:09
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Target;

/**
 * Class description:
 *
 * @author Burim
 */
abstract class TargetAbstract
{
    protected $storagePath;
    protected $processor;
    protected $userDir;
    protected $fileName;
    protected $fopConfigPath;
    protected $outputFileName;

    /**
     * @param $userDir
     * @return Pdf
     */
    public function setUserDir($userDir)
    {
        $this->userDir = $userDir;
        return $this;
    }

    /**
     * @param $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName . '.' . $this->getFormat();
        return $this;
    }

    /**
     * @return string file name
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param $layout
     * @return string
     */
    public function getStylesheet($layout)
    {
        return $this->stylesheetPath . DIRECTORY_SEPARATOR . strtolower($layout) . '.xsl';
    }

    /**
     * @param \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface $source
     * @param $layout
     * @return string
     * @throws TargetFileNotFoundException
     */
    public function getContent(
        \CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface $source,
        $layout
    ) {
        // do not generate if the file was generated and not changed
        if (!file_exists($this->getPath())) {
            $this->cleanDirectory();
            $this->processor->transform($source, $this, $layout);
        }

        if (file_exists($this->getPath())) {
            return file_get_contents($this->getPath());
        } else {
            throw new TargetFileNotFoundException('Target file not found!');
        }
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $storagePath = $this->storagePath . DIRECTORY_SEPARATOR . $this->userDir . DIRECTORY_SEPARATOR . 'pdf';
        if (!is_dir($storagePath)) {
            mkdir($storagePath, 0777, true);
        }
        return $storagePath . DIRECTORY_SEPARATOR . $this->fileName;
    }

    /**
     * Clean the Xhtml files dir
     */
    public function cleanDirectory()
    {
        $filesDirectoryPath = $this->storagePath
            . DIRECTORY_SEPARATOR . $this->userDir
            . DIRECTORY_SEPARATOR . $this->getFormat()
            . DIRECTORY_SEPARATOR . '*';

        array_map('unlink', glob($filesDirectoryPath));
    }
}
