<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-12-25 14:39
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Target;

/**
 * Class description:
 *
 * @author Burim
 */
class TargetFileNotFoundException extends \Exception
{

}
