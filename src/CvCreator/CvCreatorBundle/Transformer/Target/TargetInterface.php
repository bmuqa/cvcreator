<?php
/**
 * This file is part of the CVCreator package.
 *
 * Created by: Burim on 2012-11-19 20:09
 *
 * (c) Burim Muqa <burim@muqa.ch>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CvCreator\CvCreatorBundle\Transformer\Target;

/**
 * Interface description:
 *
 * @author Burim
 */
interface TargetInterface
{
    public function getPath();
    public function getConfig();
    public function getStylesheet($layout);
    public function getFormat();
    public function setUserDir($userDir);
    public function setFileName($fileName);
    public function getFileName();
    public function getMimeContentType();
    public function getContent(\CvCreator\CvCreatorBundle\Transformer\Source\SourceInterface $sourceDocument, $layout);
}
